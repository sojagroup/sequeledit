/**  
 * Ein Java Programm zur Anzeige und Bearbeitung von MySQL-Datenbanken.
 * Es wird eine Verbindung zu einem MySQL-Server (local, Netzwerk) hergestellt.
 *  
 * Ben�tigt die Java JRE ab Version 1.7. sowie den JDBC-Treiber ab Version 5.1.36
 * Das Programm steht unter der GPL3 Lizenz siehe: http://www.gnu.org/licenses/gpl-3.0.html
 * Das Projekt wird auf http://javaforge.com/project/JMF gehostet.
 * 
 * Die Icons stammen vom Tango Desktop Project http://tango.freedesktop.org/Tango_Icon_Library.
 * 
 * @author  M. Friedrich, D. Scholz
 * @version Beta Version 0.5.1 - 17.09.2015
 * 
 * */

package de.sequeledit;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.AbstractDocument;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

import ds.swingutils.MessageBox;
import javafx.concurrent.Worker;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.Font;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import ds.swingutils.MessageBox;

class BinaryRenderer extends DefaultTableCellRenderer {

private static final long serialVersionUID = 1L;

	private String convOutput;
	private boolean showPictures;
	private boolean recognizeImages;
	private String imgType;
	public BinaryRenderer(String output, boolean showPics, boolean recognizeImg){
		convOutput = output;
		showPictures = showPics;
		recognizeImages = recognizeImg;
	}
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	private static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	/**
	 * Checks if a byte array contains an image (jpeg, png, gif). This helper method
	 * is useful to determine if a blob data type is an image. If you get the table 
	 * data, use this method as follow: 1) get data from database: byte bytearr[] = 
	 * ResultSet.getBytes(); 2) check data: checkIfBlobIsImage(bytearr). It returns 
	 * true if the byte array start with typical image bytes.
	 * @param bytearr is the data (sql blob type) to be checked
	 * @return true if the byte array contains an image, otherwise false
	 */
	private boolean checkIfBlobIsImage(byte bytearr[]){
		StringBuffer sb = new StringBuffer();
		byte abyte0[];
        long count = 0L;
        boolean result = false;
        int l = (abyte0 = bytearr).length;
        for (int k = 0; k < l; k++)
        {
        	byte b = abyte0[k];
        	if (count == 8L)
        		break;
        	sb.append(String.format("%02X ", b));
        	count++;
        }
        if(sb.toString().startsWith("FF D8 FF")) {
        	imgType = "(jpg)";
            //System.out.println(" is JPG ");
            result = true;
        }
        else if(sb.toString().startsWith("47 49 46 38 37 61") || sb.toString().startsWith("47 49 46 38 39 61")){
        	imgType = "(gif)";
        	//System.out.println(" is GIF ");
            result = true;
        }
        else if(sb.toString().startsWith("89 50 4E 47 0D 0A 1A 0A")){
        	imgType = "(png)";
        	//System.out.println(" is PNG ");
            result = true;
        }
        return result;
	}
	@Override
	public void setValue(Object value) {
	    if (value != null) {
            boolean isImg = false;
			byte bytearr[] = (byte[])value;
			if(showPictures && checkIfBlobIsImage(bytearr)){
				super.setIcon(new ImageIcon(bytearr));
				return;
			}
			if(recognizeImages){
				isImg = checkIfBlobIsImage(bytearr);
			}
			value = "NULL";
			if(bytearr.length > 0){
		    	switch(convOutput){
			    	case "String":
			    		value = (isImg)?"Image " + imgType:new String(bytearr);
			    		break;
			    	case "Hexadezimal":
			    		value = (isImg)?"Image " + imgType:"0x" + bytesToHex(bytearr);
			    		break;
			    	case "BLOB":
			    		value = (isImg)?"Image " + imgType:"BLOB";
			    		break;
			    	case "Image":
			    		value = (isImg)?"Image " + imgType:new String(bytearr);
			    		break;
			    	default:
			    		value = (isImg)?"Image " + imgType:value;
		    	}
			}
	    }
	    else{
	    	value = "NULL";
	    }
	    super.setValue(value);
	}
}
public class SequelEdit implements ConsoleListener{


	private SequelEdit se = this;
	private boolean _showSqlStateInConsole = true;
	private boolean _isSyntaxHighlighteningOn = true;
	private JFrame _frame;
	
	private JTree _tree;
	private DefaultTreeModel _treeModel;
	
	private JTable _tableDaten;
	private JTable _tableDesc;
	private DefaultTableModel _dbTableModel = new DefaultTableModel();
	
	private JTabbedPane _tabbedPaneTableEditor;
	private JTabbedPane _tabbedPaneKonsole;
	private JButton btnTblViewRefresh;
	private DefaultTableModel _dbDescTModel = new DefaultTableModel();
	private SQLConsole _console;
	
	private JButton btnMySqlLogin;
	private JButton btnMysqllogout;
	private final String _appTitle = "SequelEdit";
	private final String[] blobtypes = {"BLOB", "LONGBLOB", "TYNYBLOB", "MEDIUMBLOB", "VARBINARY"};
	//private Vector<Vector<String>> _sqlDatensatz = null;
	
	private String _userName;
	private String _userPasswort;
	private String _userPfad;
	private db _dbServer;
	//private MySqlVerbindung _sqlVerbindung;
	private DbConnector _dbConnection;
	private JButton btnSqlEditSave;
	private JToggleButton btnSwitchAscDescOrder;
	private final int _DIVIDER_SIZE = 2;
	private String _nameRoot = "keine Verbindung";
	
	private String _filename = "";
	private int maxColWidth = 300;
	private int selectRowLimit = 1000;
	private String convertBinaryTo = "Hexadezimal";
	private boolean showPicturesFromBlob = false;
	private boolean recognizeImgFromBlob = false;
	private boolean orderByPK = true;
	private String orderType = "ASC"; // ASC | DESC
	public JPopupMenu popup;
	
	/**
	 * Start der Anwendung.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SequelEdit window = new SequelEdit();
					window._frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public SequelEdit() {
		initialize();
	}

	private void setLookAndFeel(String lookAndFeel){
		try {		
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(_frame);
		//_tableDaten.setGridColor(Color.BLACK);
		//_tableDaten.setIntercellSpacing(new Dimension(1, 1));
		if(lookAndFeel == "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"){
			_tableDaten.setGridColor(Color.GRAY);
			_tableDesc.setGridColor(Color.GRAY);
		}
		// the following method calls are necessary because of Nimbus L&F
		// (it shows no grid and resets intercell spacing by default)
		_tableDaten.setIntercellSpacing(new Dimension(10, 10));
		_tableDaten.setShowGrid(true);
		_tableDesc.setShowGrid(true);
		fitTableToContent(_tableDaten, maxColWidth);
		//_frame.pack();
	}
	
	public static <T> boolean contains(final T[] array, final T v) {
	    for (final T e : array)
	        if (e == v || v != null && v.equals(e))
	            return true;

	    return false;
	}
	public int getMaxColWidth(){
		return maxColWidth;
	}
	public void setMaxColWidth(int width){
		maxColWidth = width;
	}
	public int getSelectRowLimit(){
		return selectRowLimit;
	}
	public void setSelectRowLimit(int limit){
		selectRowLimit = limit;
	}
	public String getConvertBinaryTo(){
		return convertBinaryTo;
	}
	public void setConvertBinaryTo(String convValueString){
		convertBinaryTo = convValueString;
	}
	public boolean getShowPicturesFromBlob(){
		return showPicturesFromBlob;
	}
	public void setShowPicturesFromBlob(boolean picturesFromBlob){
		showPicturesFromBlob = picturesFromBlob;
	}
	public boolean getRecognizeImgFromBlob(){
		return recognizeImgFromBlob;
	}
	public void setRecognizeImgFromBlob(boolean recogImgFromBlob){
		recognizeImgFromBlob = recogImgFromBlob;
	}
	public boolean getOrderByPK(){
		return orderByPK;
	}
	public void setOrderByPK(boolean orderByPrimaryKey){
		btnSwitchAscDescOrder.setSelected(false);
		btnSwitchAscDescOrder.setEnabled(orderByPrimaryKey);
		orderByPK = orderByPrimaryKey;
	}

	/**
	 * Initialisierung der GUI 
	 */
	private void initialize() {


		// basic frame settings
		_frame = new JFrame(_appTitle);
		//_frame.setBounds(100, 100, 745, 544);
		_frame.setBounds(100, 100, 900, 600);
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frame.getContentPane().setLayout(new BorderLayout(0, 0));
		_frame.setLocationRelativeTo(null);
	

		//Men� wird erstellt;
		_frame.setJMenuBar(getMenu());
		
		// SQL-Konsole wird erzeugt
		_console = new SQLConsole();
		_console.addListener(this);
		
		// Komponente JTree wird erzeugt
		// JTree in einer JScrollpane
		_treeModel = new DefaultTreeModel(new DefaultMutableTreeNode(_nameRoot));
		_tree = new JTree(_treeModel);
		_tree.addTreeSelectionListener(new TreeSelectionListener() {
		    public void valueChanged(TreeSelectionEvent e) {
		        DefaultMutableTreeNode node = (DefaultMutableTreeNode)_tree.getLastSelectedPathComponent();

			    /* only if a level 2 node is selected */ 
		        if (node == null|| node.getLevel() < 1) return;

		        /* retrieve the node that was selected */ 
		        //Object nodeInfo = node.getUserObject();
		        //...
		        /* React to the node selection. */
		        if(node.getLevel()==1){
		        	try {
						_dbConnection.setDatabase(node.toString());
						//sqlEditor.append("USE " + node.toString()+";\n");
			        	_frame.setTitle(_appTitle+" (DB: " + node.toString() + ")");
			        	
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		        }
		        if(node.getLevel()==2){
		        	
		        	_frame.setTitle(_appTitle+" (DB: " + node.getParent().toString() + ")");
		        	
		    		// initialize execution timer
		    		long startTime = System.nanoTime();
		    		long elapsedTime =0;
		    	    java.text.NumberFormat f = java.text.NumberFormat.getInstance();
		    	    f.setMinimumFractionDigits(  2 );  
		    	    f.setMaximumFractionDigits(  2 ); 
		        	// Tabelle leeren
		        	_dbTableModel.setRowCount(0);
		        	_dbTableModel.setColumnCount(0);
		        	_dbConnection.setTableName(node.toString());
					
					// fill table view with sql data
					try {
						fillTableView(_dbConnection.getTableData(node.getParent().toString(), node.toString(), selectRowLimit, (orderByPK)?orderType:""));
					} catch (SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					//fillTableView(node.getParent().toString(), node.toString(), selectRowLimit);
					elapsedTime = System.nanoTime() - startTime;
					// show sql state
					if(_showSqlStateInConsole){ 
						_console.append(_dbConnection.getSqlState()+ " Complete ExecTime: (" + f.format(elapsedTime/1000000000.0) + " sec)"+"\n\n");
						_console.setNewline(); //schei� hack
					}
		        	try {
		        		// Datenbank setzen
		        		_dbConnection.setDatabase(node.getParent().toString());
		        		// Tabelle fuellen
		        		//fillDescTView(_sqlVerbindung.setSqlStatement("DESC " + node.toString(), false));
		        		fillDescTView(_dbConnection.getTableDescription(node.toString()));
		        	} catch (SQLException e1) {
		        		// TODO Auto-generated catch block
		        		e1.printStackTrace();
		        	}
		        }
		    }
		});
		JScrollPane scrollPaneTree = new JScrollPane(_tree);
		scrollPaneTree.setBounds(new Rectangle(0, 0, 200, 0));


		//JTable in einer JScrollpane
		_tableDaten = new JTable(){
			private static final long serialVersionUID = 1L;
			public boolean getScrollableTracksViewportWidth(){
				if(getPreferredSize().width < getParent().getWidth()){
					//_tableDaten.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					return true;
				}
				else{
				/*	int columnNumber = getColumnCount();
					int heigth = 0;
					for(int x = 0; x < columnNumber; x++){

				        TableColumn tableColumn = getColumnModel().getColumn(x); 
				        if(tableColumn==null) 
				            return false; 
				        int col = getColumnModel().getColumnIndex(tableColumn.getIdentifier()); 
				        int rowCount = getRowCount(); 
				        int width = (int)getTableHeader().getDefaultRenderer() 
				                .getTableCellRendererComponent(this, tableColumn.getIdentifier() 
				                        , false, false, -1, col).getPreferredSize().getWidth(); 
				        for(int row = 0; row<rowCount; row++){ 
				            int preferedWidth = (int)getCellRenderer(row, col).getTableCellRendererComponent(this, 
				                    getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth(); 
				            width = Math.max(width, preferedWidth); 
				            int preferedHeigth = (int)getCellRenderer(row, col).getTableCellRendererComponent(this, 
				                    getValueAt(row, col), false, false, row, col).getPreferredSize().getHeight(); 
				            heigth = Math.max(heigth, preferedHeigth); 
				            setRowHeight(row, heigth+ getIntercellSpacing().height);
				        } 
				        getTableHeader().setResizingColumn(tableColumn); // this line is very important 
				        //if(table.getWidth() > table.getParent().getWidth())
				        tableColumn.setWidth(width+(getIntercellSpacing().width)); 
					}
					//fitTableToContent(_tableDaten);
					//_tableDaten.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);*/
					return false;
				}
			}
			
		};
		_tableDaten.getTableHeader().addMouseListener(new ColumnFitAdapter()); 
		_tableDaten.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//_tableDaten.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		_tableDaten.setEnabled(false);
		//_tableDaten.setFillsViewportHeight(true);
		_tableDaten.setModel(_dbTableModel);	
		new TableColumnResizer(_tableDaten); // in just single line :) 
		new TableRowResizer(_tableDaten); 
		
		_tableDaten.setIntercellSpacing(new Dimension(10, 10));
		_tableDaten.setShowGrid(true);
		/*_tableDaten.setCellSelectionEnabled(true);
		_tableDaten.setColumnSelectionAllowed(true);*/
		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setViewportView(_tableDaten);
		JPanel tableViewContainer = new JPanel();
		BorderLayout border = new BorderLayout();
		tableViewContainer.setLayout(border);
		tableViewContainer.add(scrollPaneTable, BorderLayout.CENTER);

		/*********************************/
		/*                               */
		/* SQL-Script-Editor             */
		/*                               */
		/*********************************/
		//final JTextPane _textPane = new JTextPane();
	    final RSyntaxTextArea _textPane = new RSyntaxTextArea();
	    _textPane.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
	    _textPane.setCodeFoldingEnabled(true);
	    _textPane.setLineWrap(true);
	      try {
	          Theme theme = Theme.load(getClass().getResourceAsStream("/themes/sequeledit.xml"));
	          theme.apply(_textPane);
	       } catch (IOException ioe) { // Never happens
	          ioe.printStackTrace();
	       }
	    

		//SyntaxModifier syntaxModifier = new SyntaxModifier(_textPane);
		//final Clipboard clipbd = Toolkit.getDefaultToolkit().getSystemClipboard();
		//Create the popup menu.
	    popup = new JPopupMenu();
		//_textPane.add(popup);
	    final JMenuItem menuCut= new JMenuItem("Ausschneiden (Strg+X)");
	    menuCut.setFont(new Font("Monospaced", Font.PLAIN, 12));
	    menuCut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	            /*String selected = _textPane.getSelectedText();
	            if(selected==null)
	                return;
	            StringSelection clipString = new StringSelection(selected);
	            clipbd.setContents(clipString,clipString);
	            int pos1 = _textPane.getSelectionStart();
	            int pos2 = _textPane.getSelectionEnd();
	            String text = _textPane.getText();
	            StringBuffer strBuf = new StringBuffer(text);
	            strBuf.replace(pos1, pos2, ""); 
	            _textPane.setText(strBuf.toString());*/
	            
	            _textPane.cut();
			}
		});
	    popup.add(menuCut);
	    final JMenuItem menuCopy = new JMenuItem("Kopieren (Strg+C)");
	    menuCopy.setFont(new Font("Monospaced", Font.PLAIN, 12));
	    menuCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
	            /*String selected = _textPane.getSelectedText();
	            if(selected==null)
	                return;
	            StringSelection clipString = new StringSelection(selected);
	            clipbd.setContents(clipString,clipString);*/
	            _textPane.copy();
			}
		});
	    popup.add(menuCopy);
	    final JMenuItem menuPaste = new JMenuItem("Einfügen (Strg+V)");
	    menuPaste.setFont(new Font("Monospaced", Font.PLAIN, 12));
	    menuPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
            	/*Transferable contents = clipbd.getContents(null);
            	String result = "";
            	boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
            	if (hasTransferableText) {
            		try {
            			result = (String)contents.getTransferData(DataFlavor.stringFlavor);
            			
        	            _textPane.replaceSelection(result);

            	    }
            	    catch (UnsupportedFlavorException | IOException ex){
            	        System.out.println(ex);
            	        ex.printStackTrace();
            	    }
            	}*/

	            _textPane.paste();
	            //StringSelection clipString = new StringSelection(selected);
	            //clipbd.setContents(clipString,clipString);
			}
		});
	    popup.add(menuPaste);
	    final JMenuItem menuSelAll = new JMenuItem("Alles auswählen (Strg+A)");
	    menuSelAll.setFont(new Font("Monospaced", Font.PLAIN, 12));
	    menuSelAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_textPane.selectAll();
			}
		});
	    popup.add(menuSelAll);

	    popup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuCanceled(PopupMenuEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) {
				// TODO Auto-generated method stub
				menuCopy.setEnabled(_textPane.getSelectedText() != null);
				menuCut.setEnabled(_textPane.getSelectedText() != null);
				menuSelAll.setEnabled(!_textPane.getText().isEmpty());
			}

	    });
		

		//TextLineNumber tln = new TextLineNumber(_textPane);
		//JScrollPane scrollPaneText = new JScrollPane(_textPane);
		//scrollPaneText.setRowHeaderView(tln);
		//tln.setDigitAlignment(0.5f);
		//tln.setForeground(Color.gray);
	      RTextScrollPane scrollPaneText = new RTextScrollPane(_textPane);
		_tabbedPaneTableEditor = new JTabbedPane(JTabbedPane.TOP);
		_tabbedPaneTableEditor.addTab("Datenausgabe", null, tableViewContainer,"Datentabelle");
		_tabbedPaneTableEditor.addTab("SQL-Editor",null, scrollPaneText,"SQL-Editor");
		
		//Add listener to components that can bring up popup menus.
	    //MouseListener popupListener = new PopupListener(popup);
	    //_textPane.addMouseListener(popupListener);

	    
		_textPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
		_textPane.setDoubleBuffered(true);
		
		// Toolbar für Datenausgabe Tab
		JToolBar toolBarTableView = new JToolBar();
		toolBarTableView.setOrientation(SwingConstants.HORIZONTAL);
		toolBarTableView.setFloatable(false);
		tableViewContainer.add(toolBarTableView, BorderLayout.NORTH);
		
		// Tabelle aktualisieren
		btnTblViewRefresh = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/view-refresh.png")));
		btnTblViewRefresh.setToolTipText("Tabelle aktualisieren");
		btnTblViewRefresh.setEnabled(false);
		btnTblViewRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					refreshTableView();
			}
		});
		toolBarTableView.add(btnTblViewRefresh);
		
		// Table View Optionen
		JButton btnTblViewSettings = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/categories/preferences-system.png")));
		btnTblViewSettings.setToolTipText("Einstellungen für Tabellenansicht");
		btnTblViewSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SettingsDlg settingsDlg = new SettingsDlg(_frame, se);
			}
		});
		toolBarTableView.add(btnTblViewSettings);
		
		// Order ASC/DESC Optionen
		final ImageIcon btnUpIcon = new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/go-up.png"));
		final ImageIcon btnDownIcon = new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/go-down.png"));
		btnSwitchAscDescOrder = new JToggleButton(btnDownIcon);
		btnSwitchAscDescOrder.setToolTipText("Sortierungsmodus (ASC|DESC) [nur Verfügbar wenn Sortierung nach PK in Optionen aktiviert ist]");
		btnSwitchAscDescOrder.addItemListener(new ItemListener() {
			   public void itemStateChanged(ItemEvent ev) {
			      if(ev.getStateChange()==ItemEvent.SELECTED){
			  		  btnSwitchAscDescOrder.setIcon(btnUpIcon);
			    	  orderType = "DESC";
			      } else if(ev.getStateChange()==ItemEvent.DESELECTED){
			    	  btnSwitchAscDescOrder.setIcon(btnDownIcon);
			    	  orderType = "ASC";
			      }
			   }
			});
		toolBarTableView.add(btnSwitchAscDescOrder);
		
		// Toolbar für SQL-Editor Tab
		JToolBar toolBar = new JToolBar();
		toolBar.setOrientation(SwingConstants.HORIZONTAL);
		toolBar.setFloatable(false);
		scrollPaneText.setColumnHeaderView(toolBar);
		
		JButton btnSqlEditNeu = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/document-new.png")));
		btnSqlEditNeu.setToolTipText("neu anlegen");
		btnSqlEditNeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.print(_textPane.getText());
				_textPane.setText("");
				btnSqlEditSave.setEnabled(false);
				_filename = "";
			}
		});
		toolBar.add(btnSqlEditNeu);

		// Script-Datei laden
		JButton btnSqlEditLaden = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/document-open.png")));
		btnSqlEditLaden.setToolTipText("SQL-Script laden...");
		btnSqlEditLaden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop desktop = null; 
				if (Desktop.isDesktopSupported()) { 
					desktop = Desktop.getDesktop();
					JFileChooser fileChooser = new JFileChooser();
					int returnVal = fileChooser.showOpenDialog(_frame);
			        if (returnVal == JFileChooser.APPROVE_OPTION){
			        	File file = fileChooser.getSelectedFile();
		                _filename = file.getAbsolutePath();
		                file = new File(_filename);
		                //try {
		                    if (!file.canRead() || !file.isFile()){
		                		MessageBox message = new MessageBox ();
		                        message.showMessage(_filename + " ist nicht lesbar / existiert nicht!");
		                    }
		                    else{		                    
		                    	// reading the file line by line 
		                    	BufferedReader bufIn = null;
		                    	try {
		                    		String content = "";
		                    		bufIn = new BufferedReader(new FileReader(_filename));
		                    		String line = null;
		                    		while ((line = bufIn.readLine()) != null) {  
		                        		content+=line + "\n";	              
		                    		}
		                    		_textPane.setText(content);
		                    	} catch (IOException e) {
		                    		// TODO Auto-generated catch block
		                    		e.printStackTrace();
		                    	}
		        				btnSqlEditSave.setEnabled(true);
		        				btnSqlEditSave.setToolTipText("SQL-Script: " + _filename + " speichern.");
		                    }
			        	}
					}	
			}
		});
		toolBar.add(btnSqlEditLaden);
		// Script-Datei laden
		JButton btnSqlEditSaveAs = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/document-save-as.png")));
		btnSqlEditSaveAs.setToolTipText("SQL-Script speichern unter...");
		btnSqlEditSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop desktop = null; 
				if (Desktop.isDesktopSupported()) { 
					desktop = Desktop.getDesktop();
					JFileChooser fileChooser = new JFileChooser();
					int returnVal = fileChooser.showSaveDialog(_frame);
			        if (returnVal == JFileChooser.APPROVE_OPTION){
			        	File file = fileChooser.getSelectedFile();
		                _filename = file.getAbsolutePath();
		                
		                file = new File(_filename);
		                //try {
		                    if (!file.exists()){
		                    	try {
									file.createNewFile();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
		                    }

		                        
		                    BufferedWriter bufOut = null;
		                    try{
		                        bufOut = new BufferedWriter(new FileWriter(_filename));
		                        bufOut.write(_textPane.getText());
		                        bufOut.flush();

		                    } catch (IOException e) {
		                    	// TODO Auto-generated catch block
		                    	e.printStackTrace();
		                    }
		    				btnSqlEditSave.setEnabled(true);
		    				btnSqlEditSave.setToolTipText("SQL-Script: " + _filename + " speichern.");
			        	}
					}	
			}
		});
		toolBar.add(btnSqlEditSaveAs);
		btnSqlEditSave = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/document-save.png")));
		btnSqlEditSave.setToolTipText("SQL-Script: " + _filename + " speichern.");
		btnSqlEditSave.setEnabled(false);
		btnSqlEditSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

		           if(_filename.length() > 1) {  
		        	   BufferedWriter bufOut = null;
		               try{
		            	   bufOut = new BufferedWriter(new FileWriter(_filename));
		                   bufOut.write(_textPane.getText());
		                   bufOut.flush();

		                } catch (IOException e) {
		                   // TODO Auto-generated catch block
		                   e.printStackTrace();
		                }          
			        }
			}	
		});
		toolBar.add(btnSqlEditSave);
		// SQL-Script ausführen
		final JButton btnSqlCompile = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/actions/media-playback-start.png")));
		btnSqlCompile.setToolTipText("SQL-Skript ausführen...");
		btnSqlCompile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSqlCompile.setEnabled(false);
			//	  Thread thread = new Thread(){
			//		    public void run(){

				if(_console.isEditable()){
				ArrayList<String> result = null;
				ArrayList<Integer> lineNumbers = null;
				ArrayList<String> filePaths = null;
				//System.out.println("Source: " + stmt);
				try {
					BatchMode fileReader = new BatchMode(_filename, _textPane.getText());
					if(fileReader != null){
					result = fileReader.getCommands();
					lineNumbers = fileReader.getLineNumArray();
					filePaths = fileReader.getFilePaths();
					}
					//System.out.println("fuelle Array");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				Vector<Vector<Vector<Object>>> tabledata = null;
				if(result != null){
					_console.resetConsole(true);
					_console.append("execute SQL-Script...\n");
					int count = 0;
					int line = 0;
					
					for(String statement : result){
						try {
							tabledata = _dbConnection.setSqlStatement(statement, true);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(_showSqlStateInConsole){ 
							if(_dbConnection.getSqlState().startsWith("Error")){
								String error = _dbConnection.getSqlState();
								error = error.replaceAll("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near", "syntax error:");
								error = error.replaceAll("at line 1", "");
								_console.append(++count + ") " + error+ " in line: " + lineNumbers.get(line) + " in file: " + filePaths.get(line)+"\n");
							}
						}
						line++;
					}
					java.util.Date now = new java.util.Date();
					if(count >0)
						_console.append("*** " + now.toString() + " - " + count +" Error(s) in SQL-Script ***\n");
					else
						_console.append("***  " + now.toString() + " - SQL-Script executed succsessful ***\n");
					_console.setNewline(); //schei� hack
					fillTableView(tabledata);
					// Tree aktualisieren (k�nnte Performance-Probleme verursachen)
					try {
						erzeugeKnoten(_dbConnection.getListOfDatabases());
			        	_frame.setTitle(_appTitle+" (DB: " + _dbConnection.getDatabase() + ")");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					_tabbedPaneKonsole.setSelectedIndex(0);
				}
				
				}
				btnSqlCompile.setEnabled(true);
			//    }
			 // };

			 // thread.start();
			}
					    
		});
		toolBar.add(btnSqlCompile);

		// Sytax-Highlightning on/off
		JButton btnSqlEditFarbig = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/16x16/categories/applications-office.png")));
		btnSqlEditFarbig.setToolTipText("Farbschema aus/aus");
		btnSqlEditFarbig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(_isSyntaxHighlighteningOn){
					_textPane.setSyntaxEditingStyle(null);	
					_isSyntaxHighlighteningOn = false;
				}
				else{
					_textPane.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);	
					_isSyntaxHighlighteningOn = true;
				}
			}
		});
		toolBar.add(btnSqlEditFarbig);

		
		//Button-Leiste unten in einem JPanel
		JPanel panelButton = new JPanel();
		// Button Login
		btnMySqlLogin = new JButton("Verbinden");
		btnMySqlLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Logindaten holen
				LoginDatenAbfrage meinLogin = new LoginDatenAbfrage();
				if (meinLogin.askForLoginData(_frame,"localhost", "root", "root", false)) { 
					_userName = meinLogin.getUser();
					_userPasswort = meinLogin.getPassword();
					_userPfad = meinLogin.getPath();
					_dbServer = meinLogin.getDbServer();
					switch(_dbServer){
					case MYSQL:
						_dbConnection = new DbModulMariaDB(_userName, _userPasswort, _userPfad);
						_console.setPrompt("mysql> ", "    -> ");
						break;
					case SQLITE:
						_dbConnection = new DbModulSqLite(_userPfad);
						_console.setPrompt("sqlite> ", "   ...> ");
						break;
					case MSSQL:
						_dbConnection = new DbModulMsSql(_userName, _userPasswort, _userPfad);
						_console.setPrompt("mssql> ", "    -> ");
						break;
					}
					if (_dbConnection._connection != null) {
						_nameRoot = _userName + "@"+ _userPfad;
						btnMySqlLogin.setEnabled(false);
						btnMysqllogout.setEnabled(true);
						_console.setEditable(true);
						_console.resetConsole(false);
						//dialogIn myLogin = new dialogIn();
						//_sqlVerbindung = new MySqlVerbindung(_userName,_userPasswort);
						try {
							erzeugeKnoten(_dbConnection.getListOfDatabases());
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}				
			}
		});
		btnMySqlLogin.setBounds(34, 536, 130, 25);
		panelButton.add(btnMySqlLogin); 
		// Button Logout
		btnMysqllogout = new JButton("Trennen");
		btnMysqllogout.setEnabled(false);
		btnMysqllogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_nameRoot = "keine Verbindung";
				_dbConnection.closeConnection();
				_frame.setTitle(_appTitle);
				btnMySqlLogin.setEnabled(true);
				btnMysqllogout.setEnabled(false);
				
				// Tabelle leeren
			    _dbTableModel.setRowCount(0);
			    _dbTableModel.setColumnCount(0);
			    _treeModel.setRoot(new DefaultMutableTreeNode(_nameRoot));
			    
			    // SQL-Konsole leeren und sperren
				_console.setText("");
				_console.setEditable(false);
			}
		});
		btnMysqllogout.setBounds(176, 536, 130, 25);
		panelButton.add(btnMysqllogout);
		// Button Ende
		JButton btnEnde = new JButton("Programm beenden");
		btnEnde.setBounds(589, 536, 216, 25);
		btnEnde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (_dbConnection != null) 
					_dbConnection.closeConnection();
				System.exit(0);
			}
		});
		panelButton.add(btnEnde);
		
		/*************************************/
		/*                                   */
		/* SQL-Konsole                       */
		/*                                   */
		/*************************************/
		// SQL-Konsole
		JScrollPane scrollPaneEdit = new JScrollPane(_console );
		// Buttonpane neben SQL Editor
		JPanel panelSqlButton = new JPanel();

/*		// Button SQL ausfuehren
		JButton btnSQLExecute = new JButton("SQL go");
		btnSQLExecute.setFont(new Font("Dialog", Font.BOLD, 12));
		btnSQLExecute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				executeConsoleStatement();
			}
		});*/
		
		//String pngDim = "16x16";
		String pngDim = "22x22";
		panelSqlButton.setLayout(new BoxLayout(panelSqlButton, BoxLayout.PAGE_AXIS));
		// Button SQL-Fenster leeren
		JButton btnSqlLeeren = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/"+pngDim+"/actions/edit-clear.png")));
		btnSqlLeeren.setToolTipText("Konsole leeren");
		btnSqlLeeren.setFont(new Font("Dialog", Font.BOLD, 12));
		btnSqlLeeren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_console.resetConsole(true);
			}
		});
		panelSqlButton.add(btnSqlLeeren);

		// button open external source in editor 
		JButton btnSqlExterneQuelle = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/"+pngDim+"/apps/accessories-text-editor.png"))); 
		// check: os supports opening external file?  
		@SuppressWarnings("unused")
		Desktop desktop = null; 
		if (Desktop.isDesktopSupported()) {  
			btnSqlExterneQuelle.setEnabled(true);
			btnSqlExterneQuelle.setToolTipText("Skriptdatei im externen Editor laden");
		} else {
			btnSqlExterneQuelle.setEnabled(true);
			btnSqlExterneQuelle.setToolTipText("OS unterstützt das Laden einer Skriptdatei im externen Editor nicht");
		}
		btnSqlExterneQuelle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop desktop = null; 
				if (Desktop.isDesktopSupported()) { 
					desktop = Desktop.getDesktop();
					JFileChooser fileChooser = new JFileChooser();
					int returnVal = fileChooser.showOpenDialog(_frame);
			        if (returnVal == JFileChooser.APPROVE_OPTION){
			        	File file = fileChooser.getSelectedFile();
		                String fileName = file.getAbsolutePath();
		                file = new File(fileName);
		                try {
		                    desktop.open(file);	 
		                    if (_console.isEditable())
		                    	_console.append("SOURCE "+ file.getAbsolutePath() + _console.getDelimiter());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        }
				}
			}
        });
		panelSqlButton.add(btnSqlExterneQuelle);				

		
		// Button Befehle		
		JButton btnBefehle = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/"+pngDim+"/apps/help-browser.png")));
		btnBefehle.setToolTipText("Eingabehilfen anzeigen");
		btnBefehle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// setting the pos of the InputHelp
				Point startPosChild = new Point (0,0);
				if ((_frame.getExtendedState()!=JFrame.MAXIMIZED_BOTH) && (_frame.getExtendedState()!=JFrame.MAXIMIZED_HORIZ)) 
					// app-frame is not maximized (both or horizontal-> childframe not setLocationRelativeTo(null);
					startPosChild.setLocation(_frame.getWidth() +_frame.getBounds().getX(),_frame.getBounds().getY());
				else 
					startPosChild = null;

				@SuppressWarnings("unused")
				InputHelp frameInputHelp = new InputHelp(_console, startPosChild);
			}
		});
		panelSqlButton.add(btnBefehle);

		// Button Optionen		
		JButton btnSqlOptionen = new JButton(new ImageIcon(SequelEdit.class.getResource("/res/icons/"+pngDim+"/categories/preferences-system.png"))); 
		btnSqlOptionen.setEnabled(false);

		btnSqlOptionen.setToolTipText("Konsolen-Einstellungen ändern (noch nicht verfügbar");
		btnSqlOptionen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}			
		});
		panelSqlButton.add(btnSqlOptionen);
		panelSqlButton.setLayout(new BoxLayout(panelSqlButton, BoxLayout.PAGE_AXIS));

		
		// DESC-Table
		_tableDesc = new JTable();
		_tableDesc.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		_tableDesc.setEnabled(false);
		_tableDesc.setFillsViewportHeight(true);
		_tableDesc.setModel(_dbDescTModel);	
		
		JScrollPane scrollPaneDesc = new JScrollPane(_tableDesc);


		// putting the panes together
		// Borderlayout:
		//  south:  Buttonpane
		// center:  JSplitPane   
		//              links:  scrollPaneTree
		//             rechts:  splitTabKonsole - DatenTabelle 
		// 									oben: tabbedPaneTableEditor
		//												scrollPaneTable
		//												scrollPaneText
		//                                 unten: tabbedPaneKonsole		
		//           	 									1: scrollPaneDesc
		//													2: panelSqlEditorUndButton (borderlayout) 
		//                                                                     CENTER: scrollSQLEdit
		//                                                                       EAST: EditButton
		//
		//							
		//                           
		//          	rechts: Borderlayout
		//                  		  center: 
		//					 		   south: scrollPaneDescEdit
		_frame.getContentPane().add(panelButton, BorderLayout.SOUTH);
		
		// SQLEditor und Buttons zusammenf�hren
		JPanel panelSqlEditorUndButton = new JPanel(new BorderLayout());
		panelSqlEditorUndButton.add(scrollPaneEdit, BorderLayout.CENTER);
		panelSqlEditorUndButton.add(panelSqlButton, BorderLayout.EAST);
		
		// TabbedPane mit DESC und Konsole
		_tabbedPaneKonsole = new JTabbedPane(JTabbedPane.TOP);
		_tabbedPaneKonsole.addTab("SQL-Konsole", null, panelSqlEditorUndButton,"Eingabefenster für SQL-Statements");
		_tabbedPaneKonsole.addTab("Description",null, scrollPaneDesc,"Informtionen zur aktuellen Tabelle (DESC 'tabelle'");

		// DB-Table und TabbedPane zusammenf�gen
		JSplitPane splitPaneTableTabbed = new JSplitPane(JSplitPane.VERTICAL_SPLIT,_tabbedPaneTableEditor, _tabbedPaneKonsole);
		//splitPaneTableTabbed.setDividerLocation(350);
		splitPaneTableTabbed.setResizeWeight(1.0);
		splitPaneTableTabbed.setDividerSize(_DIVIDER_SIZE);

		// TreePane und splitPaneTableTab zusammenf�hren	
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,scrollPaneTree, splitPaneTableTabbed);
		splitPane.setDividerLocation(200);
		splitPane.setDividerSize(_DIVIDER_SIZE);
		
		// Tree/DESC und Table/Editor-Table zusammenf��gen 
		_frame.getContentPane().add(splitPane , BorderLayout.CENTER);

	}
	private void refreshTableView(){
		// only if there is a table
		if(_dbConnection.getTableName() != null && !_dbConnection.getTableName().isEmpty()){
	    	_dbTableModel.setRowCount(0);
	    	_dbTableModel.setColumnCount(0);
	    	try {
	    		// get complete table data with column names,
	    		// column types and rows
	    		// tableData[0][0] = Vector<Object> column names
	    		// tableData[0][1] = Vector<Object> column types
	    		// tableData[1]    = Vector<Vector<Object>> rows
	    		Vector<Vector<Vector<Object>>> tableData = _dbConnection.getTableData(_dbConnection.getDatabase(), _dbConnection.getTableName(), selectRowLimit, (orderByPK)?orderType:"");
	
	    		// set sql result to view table model 
	    		_dbTableModel.setDataVector(tableData.get(1), tableData.get(0).get(0));
	    		
	    		// iterate through column types to set view table model column to image type
	    		for(int col = 0; col < tableData.get(0).get(1).size(); col++){
	    			//System.out.println(tableData.get(0).get(1).get(col).toString());
	    			if(tableData.get(0).get(1).get(col).toString() == "IMG"){
	    
	    				_tableDaten.getColumnModel().getColumn(col).setCellRenderer(_tableDaten.getDefaultRenderer(ImageIcon.class));		
	    			}
	    			else if(contains(blobtypes, tableData.get(0).get(1).get(col).toString())){
	    				_tableDaten.getColumnModel().getColumn(col).setCellRenderer(new BinaryRenderer(convertBinaryTo, showPicturesFromBlob, recognizeImgFromBlob));
	    				//_tableDaten.getColumnModel().getColumn(col).setCellRenderer(_tableDaten.getDefaultRenderer(new BinaryRenderer()));
	    				//System.out.println(tableData.get(0).get(1).get(col).toString() + " " +col);
	    			}
	    		}
	    		// clear vectors
	    		tableData.clear();
	    		
	    		fitTableToContent(_tableDaten, maxColWidth);
	    		// Desc-Tabelle fuellen
	    		//fillDescTView(_sqlVerbindung.setSqlStatement("DESC " + _sqlVerbindung.getTableName(), false));
	    		fillDescTView(_dbConnection.getTableDescription(_dbConnection.getTableName()));
    		
	    	} catch (SQLException e1) {
	    		// TODO Auto-generated catch block
	    		e1.printStackTrace();
	    	}
		}
	}
	/**
	 * Callback from console, triggered when statement is fired in console 
	 */
	public void executeConsoleStatement(){
		String stmt = _console.getSqlStatement();
		if(stmt.contains("SOURCE ")){
			stmt = stmt.replaceAll("SOURCE ", " ");
			stmt = stmt.replaceAll(";", " ");
			stmt = stmt.trim();
			ArrayList<String> result = null;
			//System.out.println("Source: " + stmt);
			try {
				BatchMode fileReader = new BatchMode(stmt, null);
				if(fileReader != null)
				result = fileReader.getCommands();
				//System.out.println("fuelle Array");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			Vector<Vector<Vector<Object>>> tabledata = null;
			if(result != null){
				int count = 1;
				for(String statement : result){
					try {
						tabledata = _dbConnection.setSqlStatement(statement, true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(_showSqlStateInConsole){ 
						_console.append(count++ + ") " + _dbConnection.getSqlState()+"\n\n");
					}
				}
				fillTableView(tabledata);
				// Tree aktualisieren (k�nnte Performance-Probleme verursachen)
				try {
					erzeugeKnoten(_dbConnection.getListOfDatabases());
		        	_frame.setTitle(_appTitle+" (DB: " + _dbConnection.getDatabase() + ")");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		else{
		//_consHistory.add(stmt);
		//_conHistIndex= _consHistory.size();
		try {
			//System.out.println("Tabelle: " +_sqlVerbindung.getTableName());
			Vector<Vector<Vector<Object>>> tabledata = _dbConnection.setSqlStatement(_console.getSqlStatement(), true);
			if(!_dbConnection.getIsUpdateStmt()){ // Statement war Query (liefert also Datensatz)					
				fillTableView(tabledata);
        		// Desc-Tabelle fuellen
        		//fillDescTView(_sqlVerbindung.setSqlStatement("DESC " + _sqlVerbindung.getTableName(), false));
        		fillDescTView(_dbConnection.getTableDescription(_dbConnection.getTableName()));
			}
			else{ // Statement war Update (Datensatz wurde modifiziert)	
	        	_frame.setTitle(_appTitle+" (DB: " + _dbConnection.getDatabase() + ")");
				if(_dbConnection.getTableName()!= null){
					refreshTableView();
				}
			}
			if(_showSqlStateInConsole){ 
				_console.append(_dbConnection.getSqlState()+"\n\n");
			}
			//if(_sqlVerbindung.getTableName()!= "")
			//fillDescTView(_sqlVerbindung.setSqlStatement("DESC " + _sqlVerbindung.getTableName()));

			// Tree aktualisieren (k�nnte Performance-Probleme verursachen)
			erzeugeKnoten(_dbConnection.getListOfDatabases());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		}

	}
	private void fitTableToContent(JTable table, int maxColWidth){
		int columnNumber = table.getColumnCount();
		int heigth = 0;
        //if(!table.getScrollableTracksViewportWidth() /*table.getPreferredSize().width >= table.getParent().getWidth()*/){
        	//table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		for(int x = 0; x < columnNumber; x++){

        TableColumn tableColumn = table.getColumnModel().getColumn(x); 
        if(tableColumn==null) 
            return; 
        int col = table.getColumnModel().getColumnIndex(tableColumn.getIdentifier()); 
        int rowCount = table.getRowCount(); 
        int width = (int)table.getTableHeader().getDefaultRenderer() 
                .getTableCellRendererComponent(table, tableColumn.getIdentifier() 
                        , false, false, -1, col).getPreferredSize().getWidth(); 
        for(int row = 0; row<rowCount; row++){ 
            int preferedWidth = (int)table.getCellRenderer(row, col).getTableCellRendererComponent(table, 
                    table.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth(); 
            width = Math.max(width, preferedWidth); 
            int preferedHeigth = (int)table.getCellRenderer(row, col).getTableCellRendererComponent(table, 
                    table.getValueAt(row, col), false, false, row, col).getPreferredSize().getHeight(); 
            heigth = Math.max(heigth, preferedHeigth); 
            table.setRowHeight(row, heigth+ table.getIntercellSpacing().height);
        } 
        table.getTableHeader().setResizingColumn(tableColumn); // this line is very important 
        //if(table.getWidth() > table.getParent().getWidth())
        int calcWidth = width+table.getIntercellSpacing().width;
        tableColumn.setWidth((maxColWidth > 0 && calcWidth > maxColWidth)?maxColWidth:calcWidth); 
        //tableColumn.(width+(table.getIntercellSpacing().width)); 
        //System.out.println("CellSpacing " +  table.getIntercellSpacing().width);
		}
        if(table.getPreferredSize().width < table.getParent().getWidth())
        	table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
       // }
        //else
        	//table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		/*if(table.getWidth() < table.getParent().getWidth())
			table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		else
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);*/
			
	}
	/*
	 * Tabelle mit Daten fuellen
	 */
	/*private void fillTableView(String db, String table, int limit){
		//Tabelle leeren
		_dbTableModel.setRowCount(0);
		_dbTableModel.setColumnCount(0);

		Vector<Objects> params = new Vector<Objects>();


		try {
			if(_dbConnection.getTableData2(_dbTableModel, db, table, limit, new BinaryRenderer(convertBinaryTo, showPicturesFromBlob, recognizeImgFromBlob))){
				
				_tabbedPaneTableEditor.setSelectedIndex(0);
				
				fitTableToContent(_tableDaten, maxColWidth);
				
				// refresh btnTblViewRefresh enabled state in dependence of existing table
				// since not every sql stmnt that fills table view has an actual table,
				// for example: SELECT (1+1);
				btnTblViewRefresh.setEnabled(!_dbConnection.getTableName().isEmpty());
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	private void fillTableView(Vector<Vector<Vector<Object>>> data){
		
		if(data != null && !data.isEmpty()){
			//_sqlDatensatz = data;
			//Tabelle leeren
			_dbTableModel.setRowCount(0);
			_dbTableModel.setColumnCount(0);
	    
			// Tabelle fuellen
			
			
    		// get complete table data with column names,
    		// column types and rows
    		// data[0][0] = Vector<Object> column names
    		// data[0][1] = Vector<Object> column types
    		// data[1]    = Vector<Vector<Object>> rows

    		// set sql result to view table model 
    		_dbTableModel.setDataVector(data.get(1), data.get(0).get(0));
    		
    		// iterate through column types to set view table model column to image type
    		for(int col = 0; col < data.get(0).get(1).size(); col++){
    			//System.out.println(tableData.get(0).get(1).get(col).toString());
    			if(contains(blobtypes, data.get(0).get(1).get(col).toString())){
    				_tableDaten.getColumnModel().getColumn(col).setCellRenderer(new BinaryRenderer(convertBinaryTo, showPicturesFromBlob, recognizeImgFromBlob));
    			}
    		}
    		_tabbedPaneTableEditor.setTitleAt(0, (!_dbConnection.getTableName().isEmpty())?_dbConnection.getTableName():"Datenausgabe");
    		_tabbedPaneTableEditor.setSelectedIndex(0);
    		
			fitTableToContent(_tableDaten, maxColWidth);
			
			// refresh btnTblViewRefresh enabled state in dependence of existing table
			// since not every sql stmnt that fills table view has an actual table,
			// for example: SELECT (1+1);
			btnTblViewRefresh.setEnabled(!_dbConnection.getTableName().isEmpty());
		}
	}
	private void fillDescTView(Vector<Vector<Vector<Object>>> data){
		if(data !=null && !data.isEmpty()){
			//Tabelle leeren
			_dbDescTModel.setRowCount(0);
			_dbDescTModel.setColumnCount(0);
			
			// Tabelle fuellen
			_dbDescTModel.setDataVector(data.get(1), data.get(0).get(0));
		}
	}
	
	/**
	 * Methode erstellt aus den verfügbaren Datenbanknamen und den dazu gehörenden
	 * Tabellen eine Baumstruktur für ein JTree
	 * 
	 * @param listeDatenbanken  enthält alle verfügbaren Datenbanken
	 * 
	 * @throws SQLException
	 */
	private void erzeugeKnoten(List<String> listeDatenbanken) throws SQLException {
		DefaultMutableTreeNode nodeRoot = new DefaultMutableTreeNode(_nameRoot);
		DefaultMutableTreeNode nodeDatenbanken = null;
	    DefaultMutableTreeNode nodeTabellen = null;
	    List<String> listeTabs = null;
	    
	    for (String elementDB : listeDatenbanken) {
		    
	    	// Datenbank eintragen
	    	nodeDatenbanken = new DefaultMutableTreeNode(elementDB);
		    nodeRoot.add(nodeDatenbanken);
		    
	    	// Tabelle zur Datenbank eintragen
		    listeTabs = _dbConnection.getListOfTables(elementDB);
		    for (String elementTab : listeTabs) {
		    	nodeTabellen = new DefaultMutableTreeNode(elementTab);
		    	nodeDatenbanken.add(nodeTabellen);
		    }
	    }
	    // Tree expandieren (nur 1. Ebene)
	    _treeModel.setRoot(nodeRoot);
	    _tree.expandPath(new TreePath(nodeRoot.getPath()));
	}
	
	
	/**
	 * creates the Menu
	 * 
	 * @return JMenu (the whole menu)
	 */
	
	private JMenuBar getMenu() {
		// menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu;
		JMenuItem menuItem;

		// first menu 'Programm'.
		menu = new JMenu("Programm");
		menuBar.add(menu);
				
		menuItem = new JMenuItem(new AbstractAction("Info") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MessageBox meinPopUp = new MessageBox();
				String message =_appTitle+" Beta Version 0.5.3 - 06.05.2016\n\n" 
						+ "SequelEdit ist ein SQL-Client zur Anzeige und Bearbeitung\n"
						+ "von MySQL/MariaDB- und SQLite-Datenbanken\n\n"
						+ "SequelEdit basiert auf:\n"
						+ "    - MySQL-Treiber: MariaDB Connector/J v1.4.3\n"
						+ "      (GNU Lesser General Public License Version 2.1)\n"
						+ "    - SQLite-Treiber: SQLite-JDBC v2.5.7\n"
						+ "      (Apache License Version 2)\n"
						+ "    - MS-SQL-Server-Treiber: jtds v1.31\n"
						+ "      (GNU Lesser General Public License)\n"
						+ "    - Editor: RSyntaxTextArea v2.5.7 (modifiziert)\n"
						+ "      (modified BSD license)\n"
						+ "    - Icons: Tango v. 0.8.90\n"
						+ "      (Attribution-Non-Commercial 3.0 Netherlands)\n"
						+ "    - JTattoo v1.6.11 (MH Software-Entwicklung)\n"
						+ "      (GNU General Public License version 2.0)\n\n"
						+ "Autoren: Friedrich/Scholz\n\n"
						+ "Das Programm steht unter der GPL3 Lizenz:\n"
						+ "http://www.gnu.org/licenses/gpl-3.0.html\n\n"
						+ "Projektlink: http://bitbucket.com/sojagroup/sequeledit\n\n";
				meinPopUp.showMessage(message, _appTitle+" Beta Version 0.5.3");
		 	}
		});
		menu.add(menuItem);
		menu.addSeparator();
		
		menuItem = new JMenuItem(new AbstractAction("Default Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MetalLookAndFeel.setCurrentTheme(new DefaultMetalTheme());
				setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Ocean Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				MetalLookAndFeel.setCurrentTheme(new OceanTheme());
				setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Nimbus Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Acryl Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				// setTheme(String themeName, String licenseKey, String logoString)
	            com.jtattoo.plaf.acryl.AcrylLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Aero Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
	            com.jtattoo.plaf.aero.AeroLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Aluminium Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
	            com.jtattoo.plaf.aluminium.AluminiumLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Bernstein Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.bernstein.BernsteinLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Fast Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.fast.FastLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Graphite Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.graphite.GraphiteLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("HiFi Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.hifi.HiFiLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Luna Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.luna.LunaLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.luna.LunaLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("McWin Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.mcwin.McWinLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Mint Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.mint.MintLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Noire Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.noire.NoireLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Smart Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.smart.SmartLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menuItem = new JMenuItem(new AbstractAction("Texture Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				com.jtattoo.plaf.texture.TextureLookAndFeel.setTheme("Default", "INSERT YOUR LICENSE KEY HERE", "SequelEdit");
				setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
		 	}
		});
		//menu.add(menuItem); // seems buggy on Konsole background color
		menuItem = new JMenuItem(new AbstractAction("CDE/Motiv Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		
		menuItem = new JMenuItem(new AbstractAction("GTK+ Style") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		 	}
		});
		menu.add(menuItem);
		menu.addSeparator();
		menuItem = new JMenuItem(new AbstractAction("Beenden") {
		private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				if (_dbConnection!=null) 
					_dbConnection.closeConnection();
				System.exit(0);
		 	}
		});
		menu.add(menuItem);
		
		return menuBar;
	}
}