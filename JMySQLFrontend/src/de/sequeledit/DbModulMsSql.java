package de.sequeledit;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;

import ds.swingutils.MessageBox;

/**
 * This class is the MariaDB modul for communication via
 * mariadb-java-client-1.4.3.jar driver with MariaDB/MySQL databases.
 * It extends from DbConnector class. 
 * @author DiTo, MiFri
 *
 */
public class DbModulMsSql extends DbConnector {
	//public Connection _connection = null;
	//private Vector<Vector<String>> _result = new Vector<Vector<String>>();
	//private int _columnsNumber;
	//private String _tableName;
	//private String _sqlState;
	//private boolean _updateStatement = false;

	
	/**
	 * Constructor	
	 * @param userName user name for auhentification
	 * @param pw password for auhentification
	 * @param pfad path for connection
	 */
	public DbModulMsSql (String userName, String pw, String pfad) {
		if (!connectToDb(userName, pw, pfad, null)) {
			// keine verbindung!
		}
	}
	
	/**
	 * Constructor
	 * @param userName user name for auhentification
	 * @param pw password for authentification
	 * @param pfad path for connection
	 * @param datenbank database
	 */
	public DbModulMsSql (String userName, String pw, String pfad, String datenbank) {
		if (!connectToDb(userName, pw, pfad, datenbank)) {
			// keine verbindung!
		}
	}
	
	/**
	 * Try to connect to MariaDB/MySQL database. This method takes user name, password,
	 * path and optional database for authentification and connection to database server.
	 * @param userName user name
	 * @param pw password, may be empty if no password is needed
	 * @param pfad url to database server
	 * @param datenbank database to connect to, can be null
	 * @return true if connection to database is successful otherwise false
	 */
	private  Boolean connectToDb(String userName, String pw, String pfad, String datenbank) {
		try {
			// Laden des MySQL-Treibers
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			//Class.forName("com.mysql.jdbc.Driver");
			
			// Herstellen der Verbindung
			String url = "";
			if (datenbank !=null)  
				//url = "jdbc:mysql://localhost/" + datenbank + "?user=" + userName + "&password=" + pw;
				url = "jdbc:jtds:sqlserver://" + pfad + ";" + "user=" + userName + ";password=" + pw + ";database=" + datenbank;
			else
				//url = "jdbc:mysql://localhost/?user="+ userName + "&password=" +pw;
				url = "jdbc:jtds:sqlserver://" + pfad + ";" + "user="+ userName + ";password=" +pw;
			
			_connection = DriverManager.getConnection(url);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			MessageBox meinPopUp = new MessageBox();
			meinPopUp.showMessage("Der JDBC-Treiber kann nicht geladen werden!");
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			MessageBox meinPopUp = new MessageBox();
			meinPopUp.showMessage("Es kann keine Verbindung für den Benutzer "+ userName + " hergestellt werden!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Returns list of databases (database names as String) from the current connection. 
	 * @return List of Strings containing all database names
	 * @throws SQLException
	 */
	public List<String> getListOfDatabases() throws SQLException {
		List<String> result = new ArrayList<String>();
		
		ResultSet recSet = _connection.getMetaData().getCatalogs();
		while (recSet.next()) {
			result.add(recSet.getString("TABLE_CAT"));
		}
		recSet.close();
		return result ;
	}

	/**
	 * Returns all tables in specified database as ArrayList of Strings.
	 * @param datenbank - database which contains tables 
	 * @return List of Strings containing all table names
	 * @throws SQLException
	 */
	public List<String> getListOfTables(String datenbank) throws SQLException {
		List<String> result = new ArrayList<String>();
		//String[] types = {"TABLE"};
		ResultSet recSet = _connection.getMetaData().getTables(datenbank, null, "%", null);

		// java.sql.Statement stmt = _connection.createStatement();
		// ResultSet recSet = stmt.executeQuery("Select table_name FROM user_tables");
		while (recSet.next()) {
			  //result.add(recSet.getString(3));
			  result.add(recSet.getString("TABLE_NAME"));
		}
		recSet.close();
		return result ;
	}
		
	/**
	 * Returns column names of specified table in specified database as ArrayList of Strings.
	 * @param datenbank - database which contains table
	 * @param tabelle - table to retrieve column names from
	 * @return List of Strings containing all column names
	 * @throws SQLException
	 */
	public List<String> getListOfColumns(String datenbank, String tabelle) throws SQLException {
		List<String> result = new ArrayList<String>();
		
		ResultSet resSet = this._connection.getMetaData().getColumns(datenbank, null, tabelle, null);
		while (resSet.next()) {
			result.add(resSet.getString("COLUMN_NAME"));
		}
		this._connection.setCatalog(datenbank);
		resSet.close();
		return result ;
	}
	
	/**
	 * Returns complete table with column names, column types, and rows. 
	 * @param db is the database for which the data will be returned
	 * @param tabelle is the table for which the data will be returned
	 * @param spalte is the column of the table, only the data for this column will be returned 
	 * @return the result data for the table as vector of vector of vector of objects. 
	 */
	public Vector<Vector<Vector<Object>>> getTableData(String db, String tabelle, int limit) throws SQLException {
		// check if connected else reconnect
		//checkAndReconnect();
		
		// set database
		this._connection.setCatalog(db);
		_tableName = tabelle;
		
		// create Vector for result
		Vector<Vector<Vector<Object>>> result = new Vector<Vector<Vector<Object>>>();

		// create new sql statement
		java.sql.Statement stmt = this._connection.createStatement();
		
		// create new result set
		ResultSet resSet;
		
		// prepare row limit string if any
		String rowLimit = (limit > 0)?" LIMIT " + String.valueOf(limit):"";
		
		// execute select query and save in result set
		resSet = stmt.executeQuery("SELECT * FROM " + "`"+tabelle+"`" + rowLimit);
		
		//System.out.println("SELECT * FROM " + "`"+tabelle+"`" + rowLimit);
		// get meta data
		java.sql.ResultSetMetaData rsmd=resSet.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		
		// add new meta data vector [0]
		result.add(new Vector<Vector<Object>>());
		// add new col names vector [0][0]
		result.get(0).add(new Vector<Object>());
		// add new col types vector [0][1]
		result.get(0).add(new Vector<Object>());
		
		// get column names and types
		for(int x = 1; x <= columnsNumber; x++){
			result.get(0).get(0).add(rsmd.getColumnName(x));
			result.get(0).get(1).add(rsmd.getColumnTypeName(x));
		}
		
		// create new table data vector [1]
		result.add(new Vector<Vector<Object>>());
		
		// iterate through rows in result set
		int row = 0;
		while (resSet.next()) {
			// add new row vector [1][row]
			result.get(1).add(row, new Vector<Object>());

			for(int col=1; col <= rsmd.getColumnCount(); col++){
				result.get(1).get(row).add(resSet.getObject(col));
			}
			row++;				
		}
		resSet.close();
		return result;
	}

	/**
	 * Returns all fields for a given column in a table. 
	 * @param tabelle is the table for which the data will be returned
	 * @param spalte is the column of the table, only the data for this column will be returned 
	 * @return the result data for the specified column of the table as vector of Strings. 
	 */
	public Vector<Object> getColumnData(String tabelle, String spalte, int limit) throws SQLException {
		_tableName = tabelle;
		Vector<Object> result = new Vector<Object>();
		result.clear();
		
		java.sql.Statement stmt = this._connection.createStatement();
		ResultSet resSet;
		
		String rowLimit = (limit > 0)?" LIMIT " + String.valueOf(limit):"";
		resSet = stmt.executeQuery("SELECT " + spalte + " FROM " + "`"+tabelle+"`" + rowLimit);

		java.sql.ResultSetMetaData rsmd=resSet.getMetaData();

		while (resSet.next()) {
			switch(rsmd.getColumnTypeName(1)){
			case "TINYBLOB":
			case "BLOB":
			case "MEDIUMBLOB":
			case "LONGBLOB":
				byte bytearr[] = resSet.getBytes(spalte);
				if(checkIfBlobIsImage(bytearr)){
					ImageIcon img =  new ImageIcon(bytearr);
					result.add(img);
					setColumnTypeIsImg(true);
				}
				else{
			    	//Object field = resSet.getObject(spalte);
			    	result.add(bytearr);
					setColumnTypeIsImg(false);
				}
				break;
			default:
		    	Object field = resSet.getObject(spalte);
		    	result.add(field);
				setColumnTypeIsImg(false);
			}
					
		}
		resSet.close();
		return result;
	}
	
	/**
	 * Returns the description table for a given table. 
	 * @param tableName specifies the name of the table for which the description table will be returned
	 * @return the description table as 2 dimensional vector
	 */
	public Vector<Vector<Vector<Object>>> getTableDescription(String tableName) throws SQLException {
		return setSqlStatement("DESC " + tableName, false);
	}
	
	/**
	 * Executes sql statement. If the statement returns result set (is query), 
	 * the resulting table has to be represented in a 3 dimensional Vector of 
	 * the following form:
	 * result[0][0][0...n]: all column names.	
	 * result[0][1][0...n]: all column types.
	 * result[1][0...n (row)][0...n (field)]: each row as Vector of Objects.
	 * @param statement - sql statement to be executed
	 * @param showState - if true, status message for executed statement will be generated, use getSqlState() afterwards.
	 * @return - 3 dimensional Vector of Objects containing table data | null.
	 * @throws SQLException
	 */	
	public Vector<Vector<Vector<Object>>> setSqlStatement(String statement, boolean showState) throws SQLException {
		// check if connected else reconnect
		//checkAndReconnect();
		
		// initialize update stmnt to false
		_updateStatement = false;
		
		// initialize execution timer
		long startTime = System.nanoTime();
		long elapsedTime =0;
	    java.text.NumberFormat f = java.text.NumberFormat.getInstance();
	    f.setMinimumFractionDigits(  2 );  
	    f.setMaximumFractionDigits(  2 );  
	    
	    // create Vector for result
	    _result = new Vector<Vector<Vector<Object>>>();
		java.sql.Statement stmt = this._connection.createStatement();

		// reset last Query Statement
		_lastQueryStmt = "";
		
		try{
			// if stmnt has result set...
			if(stmt.execute(statement)){
				//System.out.println(statement);
				ResultSet resSet;
				resSet = stmt.getResultSet();
				java.sql.ResultSetMetaData rsmd=resSet.getMetaData();
				_columnsNumber = rsmd.getColumnCount();
				// set last Query Statement
				_lastQueryStmt = statement;
				if(_columnsNumber == 0)
					return _result;
				else{
					if(!statement.contains("DESC "))
						_tableName = rsmd.getTableName(1);
					
					// add new meta data vector [0]
					_result.add(new Vector<Vector<Object>>());
					// add new col names vector [0][0]
					_result.get(0).add(new Vector<Object>());
					// add new col types vector [0][1]
					_result.get(0).add(new Vector<Object>());
					
					// get column names and types
					for(int x = 1; x <= _columnsNumber; x++){
						_result.get(0).get(0).add(rsmd.getColumnName(x));
						_result.get(0).get(1).add(rsmd.getColumnTypeName(x));
					}
					
					// create new table data vector [1]
					_result.add(new Vector<Vector<Object>>());
					
					// iterate through rows in result set
					int row = 0;
					while (resSet.next()) {
						// add new row vector [1][row]
						_result.get(1).add(row, new Vector<Object>());

						for(int col=1; col <= _columnsNumber; col++){
							_result.get(1).get(row).add(resSet.getObject(col));
						}
						row++;				
					}
				
					elapsedTime = System.nanoTime() - startTime;
					//resSet.last();
					int size = resSet.getRow();
					//resSet.beforeFirst();
					if(showState){
						if(size == 0){
							_sqlState = "Empty set (" + f.format(elapsedTime/1000000000.0) + " sec)";
						}
						else if(size == 1){
							_sqlState = size + " row in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}
						else{
							_sqlState = size + " rows in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}	
					}
				}
				resSet.close();
			}
			else{
				if(statement.contains("USE ")){
					setDatabase(statement.substring(statement.indexOf(" "), statement.length()-1).trim());
				}
			
				//_updateStatement = true;
				int rows = stmt.getUpdateCount();
			
				setUpdateStmt(statement);
			
				if(showState){
					if(rows == 1){
						_sqlState = "Query OK, " + rows + " row affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
					else{
						_sqlState = "Query OK, " + rows + " rows affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
				}
			}
		}
		catch(SQLException ex){
			// handle any errors
			if(showState)
				_sqlState = "Error " + ex.getErrorCode() + " (" + ex.getSQLState() + "): " + ex.getMessage() + " " + SQLError.mysqlToXOpen(ex.getErrorCode()) + " : " +SQLError.get(SQLError.mysqlToXOpen(ex.getErrorCode()));
		}
		finally{
	
		}
		return _result;
	}
}

