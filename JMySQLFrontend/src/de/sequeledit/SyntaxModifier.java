package de.sequeledit;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.text.*;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.StyledDocument;


public class SyntaxModifier{
	
	private JTextPane _textPane;

	private ArrayList<String> dictionary = new ArrayList<String>(Arrays.asList("create", "select", "database", "from",
			"where", "add", "all", "distinct", "update", "set", "between", "like", "and", "inner", "join", 
			"union", "left", "right", "asc", "desc", "having", "order by", "group by", "table", "alter", "index",
			"on", "insert", "into", "delete", "values", "show", "tables", "drop", "use", "decimal", "varchar",
			"date", "char", "year", "time", "binary", "int", "integer", "tinyint", "smallint", "mediumint", "real",
			"bigint", "float", "databases", "auto_increment", "primary key", "as", "having", "foreign key", "references",
			"source", "curdate", "curtime", "delimiter", "procedure", "begin", "end", "call", "status"));
    private int currentIndexOfSpace, tW, tH;
    private String typedWord;
    private StyledDocument styledDoc;
    private AbstractDocument doc;

    private DocumentFilter documentFilter = new DocumentFilter() {
    	@Override
    	public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
            //checkForAndShowSuggestions();

    		System.out.println("insert "+ str); // Micha
    		super.insertString(fb, offs, str, a);
            //checkForAndShowSuggestions();
    		
    	}
    	@Override
    	public void remove(FilterBypass fb, int offset, int length)throws BadLocationException {
    		System.out.println("remove "); // Micha
    		super.remove(fb, offset, length);
            //checkForAndShowSuggestions();
    		
    	}
    	@Override
    	public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException{

            //if(text == " ")
            	//checkSyntax(text);
    		checkForAndShowSuggestions();
    		//System.out.println("replace "+ text + "offset: " + offset); // Micha
    		super.replace(fb, offset, length, text, attrs);
    	}

    };
	public SyntaxModifier(JTextPane textPane){
		_textPane = textPane;
		//_textPane.setDocument(doc);
	    styledDoc = _textPane.getStyledDocument();
	    doc = (AbstractDocument)styledDoc;
	    doc.setDocumentFilter(documentFilter);
		//( (AbstractDocument) _textPane.getDocument()).setDocumentFilter(documentFilter);

        //create necessary styles for various characters
        javax.swing.text.Style style = _textPane.addStyle("Red", null);
        StyleConstants.setForeground(style, Color.RED);
        javax.swing.text.Style style2 = _textPane.addStyle("Blue", null);
        StyleConstants.setForeground(style2, Color.BLUE);
		//setDictionary(words);
	}
	String tmp = " ";
	private void checkSyntax(String input){
		tmp += input;
		if(tmp.startsWith("(\r|\n)") || tmp.startsWith(" ")){

			if(tmp.length() > 1 && (tmp.endsWith("\r") || tmp.endsWith("\n") || tmp.endsWith(" "))){
				tmp = tmp.replace("\n", " ");
				tmp = tmp.replace("\r", " ");
				System.out.println(tmp.trim());
				tmp = " ";
			}
		}
		else
			tmp = "";
	}
    private void checkForAndShowSuggestions() {
        typedWord = getCurrentlyTypedWord();

        //suggestionsPanel.removeAll();//remove previos words/jlabels that were added

        //used to calcualte size of JWindow as new Jlabels are added
        tW = 0;
        tH = 0;

        wordTyped(typedWord);

        /*if (!added) {
            if (autoSuggestionPopUpWindow.isVisible()) {
                autoSuggestionPopUpWindow.setVisible(false);
            }
        } else {
            showPopUpWindow();
            setFocusToTextField();
        }*/
    }
    public void setDictionary(ArrayList<String> words) {
        dictionary.clear();
        if (words == null) {
            return;//so we can call constructor with null value for dictionary without exception thrown
        }
        for (String word : words) {
            dictionary.add(word);
        }
    }
    public String getCurrentlyTypedWord() {//get newest word after last white spaceif any or the first word if no white spaces
        String text = _textPane.getText();
        String wordBeingTyped = "";
        text = text.replaceAll("(\\r|\\n)", " ");
        if (text.contains(" ")) {
            int tmp = text.lastIndexOf(" ");
            if (tmp >= currentIndexOfSpace) {
                currentIndexOfSpace = tmp;
                wordBeingTyped = text.substring(text.lastIndexOf(" "));
            }
        } else {
            wordBeingTyped = text;
        }
        return wordBeingTyped.trim();
    }
    private String wordTyped(String typedWord) {

    	
        if (typedWord.isEmpty()) {
            return "";
        }
        //System.out.println("Typed word: " + typedWord); // Micha

        boolean suggestionAdded = false;

        /*for (String word : dictionary) {//get words in the dictionary which we added
            boolean fullymatches = true;
            for (int i = 0; i < typedWord.length(); i++) {//each string in the word
                if (!typedWord.toLowerCase().startsWith(String.valueOf(word.toLowerCase().charAt(i)), i)) {//check for match
                    fullymatches = false;
                    break;
                }
            }
            if (fullymatches) {
                //addWordToSuggestions(word);
                suggestionAdded = true;
                System.out.println("Typed word: " + typedWord); // Micha
            }
        }*/
        for (String word : dictionary) {//get words in the dictionary which we added
            boolean fullymatches = false;
                if (typedWord.toLowerCase().equals(word)) {//check for match
                    fullymatches = true;
                    //break;
                }

            if (fullymatches) {
                //addWordToSuggestions(word);
                suggestionAdded = true;
                //String suggestedWord = getText();
                System.out.println("Typed word: " + typedWord); // Micha
                String text = _textPane.getText();
                String t = text.substring(0, text.lastIndexOf(typedWord));
                String tmp = t + text.substring(text.lastIndexOf(typedWord)).replace(typedWord, typedWord.toUpperCase());
                _textPane.setText(t);
                StyleContext sc = StyleContext.getDefaultStyleContext();
                AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.RED);
                AttributeSet aset2 = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.BLACK);
                //aset = sc.addAttribute(aset, StyleConstants.FontFamily, f);

                //int len = tPane.getDocument().getLength();
                _textPane.setCaretPosition(text.lastIndexOf(typedWord));
                _textPane.setCharacterAttributes(aset, false);
                _textPane.replaceSelection(typedWord.toUpperCase());
                _textPane.setCharacterAttributes(aset2, false);
               // styledDoc.setCharacterAttributes(text.lastIndexOf(typedWord), typedWord.length(), aset/*_textPane.getStyle("Red")*/, false);
            }
        }
        return typedWord;
    }

}
