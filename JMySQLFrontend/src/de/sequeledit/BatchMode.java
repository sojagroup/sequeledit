package de.sequeledit;

/** Class reads a file with multi commands (SQL)
 *  deletes empty lines and commented lines and commented areas
 *  creates a ArrayList with all commands entry by entry 
 *     
 *  @author M.Friedrich, D.Scholz, 2015
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import ds.swingutils.MessageBox;

public class BatchMode {

	private ArrayList<String> _commands = new ArrayList<String>();
	private ArrayList<String> _files = new ArrayList<String>();
	private ArrayList<Integer> _lineNumbers = new ArrayList<>();
	boolean multiLineComment = false;
	
	/**
	 * Reading a file with multi commands (SQL), extracting commands and putting them to an ArrayList.
	 * There are two ways of use: (1) as file parser with parameter text set to null, (2) as string
	 * parser.  
	 * 
	 * @param pathToFile -full path expected textfile, text has to be null for file parsing
	 * @param text -a string to parse (pathToFile can be empty string for non existing file), 
	 * set to null for file parsing 
	 * @throws FileNotFoundException
	 */
	public BatchMode (String pathToFile, String text) throws FileNotFoundException {
		// deciding what to do
		if (pathToFile != null && text == null) {
			//loading a file -> checking the file
			parse(getFileContent(pathToFile), pathToFile);
			  //System.out.println("from File");
		} else if (text != null && !text.trim().isEmpty()) {
			//working with the given string
			parse(text, pathToFile);
			  //System.out.println("from String");
		} 
    } 
	
	
	public ArrayList<String> getCommands() {
		return _commands;
	}
	public ArrayList<String> getFilePaths() {
		return _files;
	}
	public ArrayList<Integer>  getLineNumArray(){
		return _lineNumbers;
	}
	
	private String getFileContent(String pathToFile) {
		String result ="";
		MessageBox message = new MessageBox ();
		
		File file = new File(pathToFile);
		if (!file.canRead() || !file.isFile()) {
            message.showMessage(pathToFile + " ist nicht lesbar / existiert nicht!");
        	return null;
		} else {
			BufferedReader bufIn = null;
        	// reading the file line by line 
        	try {
        		bufIn = new BufferedReader(new FileReader(pathToFile));
        		String line = null;
        		while ((line = bufIn.readLine()) != null) {
        			// no empty lines and no lines starting with "--" 
        			//if ((line.length()>0) && (!line.startsWith("--")) && (!line.startsWith("#")))  
        			result+=" " + line + '\n';
        		}
        			bufIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bufIn != null) {
                	try {
                		bufIn.close();
                	} catch (IOException e) {
                		
                	}
                }
            }
	    }
		return result;
	}
	
	/**
	 * neue Parserfunktion mit Zeilennummern
	 * 
	 * Parser ruft sich rekursiv bei 'source'-Statements auf
	 * 
	 * ToDo:
	 * - delimiter switches müssen single line sein (wie in SQL-CLI)
	 * - SOURCE-Statements Multiline-fähig machen (momentan nur in einer Zeile zulässig)
	 * - wenn SOURCE-Pfad nicht existiert wird MessageBox angezeigt, statt SQL-Meldung in Konsole
	 */
	enum states { init, mlc, mls, eolc };
	private void parse2(String text, String file){
		states state = states.init;
		
		int posStartRead = 0;
		int posEndOfLine = 0;
		int posMlcStart = 0;
		int posMlcEnd = 0;
		int posEolComment = 0;
		boolean endOfString = false;
		
		String[] lines = text.split("\n", -1);
		
		for(String line : lines){
			System.out.println("line: " + line);
			String[] stmtsInLine = text.split(";", 0);
			for(String statement : stmtsInLine){
				//statement = " " + statement.subSequence(posStartRead, posEndOfLine);
				System.out.println("line: " + line);
			}
		}
		
		/*while(true){
			// extract one complete line
			posEndOfLine = text.indexOf('\n', posStartRead);
			if(posEndOfLine == -1){
				posEndOfLine = text.length();
				endOfString = true;
			}
			else
				posStartRead = posEndOfLine+1;
			
			
			if(endOfString) break;
		}*/
		
	}
	private void parse (String text, String file){
		String stmt = "";
		int stmntCount = 0;
		
		if(text != null && !text.trim().isEmpty()){
			//System.out.println("parse");
			int count = 0;
			String dlimit = ";";
			String[] lines = text.split("\n", -1);
			
			for(String line : lines){

				count++; //line counter
				line = " " + line;	
				// remove comments
				if(multiLineComment){
					line = checkForMultiLineCommentEnd(line); // removes '...*/' 
						
				}
				else{
					line  = checkForMultiLineCommentStart(line); // removes '/*...' and '/*...*/'
					line = removeOneLineComment(line); // removes '-- ...' and '#...' comments
				}
					
				// check for source (not multiline safe atm)
				if(line.toLowerCase().contains(" source "))
				{
					int p = line.toLowerCase().indexOf(" source ");
					int p2 = line.indexOf(dlimit, p);
					String pathToFile = line.substring(p+7, p2).trim();
					pathToFile = pathToFile.toLowerCase().replace("source", "").trim();
					String fileContent = getFileContent(pathToFile);
					// call parse() recursively
					if(fileContent != null && !fileContent.isEmpty())
						parse(fileContent, pathToFile);
					line = "";
				}
					
				// check for delimiter switch (not multiline safe atm)
				if(line.toLowerCase().contains(" delimiter "))
				{
					int p = line.toLowerCase().indexOf(" delimiter ");
					int p2 = line.length();
					dlimit = line.substring(p+10, p2).trim();
					line = "";
				}				
					
				// check for end of statement
				if(line.contains(dlimit))
				{
					String[] lineStatements = getStatementsFromLine(line, dlimit);
					for(String statement : lineStatements){
						stmt += statement;
						stmt = stmt.replace(dlimit, ";");
						stmt = removeControlSigns(stmt, true); // looks nice, bad performance

						if(statement.contains(dlimit)){
							_commands.add(stmt.trim());
							_files.add(file);
							_lineNumbers.add(count-stmntCount);
							//System.out.println(stmt);
							stmt = "";
							stmntCount = 0;
						}
						else{
							stmt = statement;
							stmntCount = 1;
						}
					}
				}
				else{ // multiline statement
					if(line.trim().length()>0|| stmntCount >0){
						stmntCount++;
						stmt += line;
					}
				}
			}
			// flush last statement without delimiter (to trigger error) 
			if(!stmt.isEmpty()){
				count++;
				_commands.add(stmt.trim());
				_files.add(file);
				_lineNumbers.add(count-stmntCount);
			}
		}
	}
	private String[] getStatementsFromLine(String line, String dlimit){
		ArrayList<String> statements = new ArrayList<>();
		if(line != null){
			StringBuffer strBuf = new StringBuffer(line);
			int pos1=0;
			int pos2=0;	
			boolean end = false;
			while(true){
				pos2 = strBuf.indexOf(dlimit, pos1); // looking for statement delimiter 
				if (pos2==-1){
					pos2 = strBuf.length(); // end of line
					if (strBuf.substring(pos1, pos2).trim().isEmpty())
						break; // empty after last delimiter		
					pos2-=dlimit.length();
					end = true;
				}
				statements.add(strBuf.substring(pos1, pos2+dlimit.length()));
				pos1 = pos2 + dlimit.length(); // set new startpoint after last delimiter
				if(end) break;
			}
		}
		return statements.toArray(new String[0]);
	}
	private String removeControlSigns(String text, boolean shrinkSpaces){
		if(text != null){
			// remove control signs from line, for each one insert space
			text = text.replace("\n", " ");
			text = text.replace("\r", " ");
			text = text.replace("\t", " ");	
			if(shrinkSpaces){
				int pos1=0;
				while(true){

					pos1 = text.indexOf("  "); // looking for double space
					if (pos1==-1){
						break; // not found, end
					}
					text = text.replace("  ", " ");
				}
			}
		}
		return text;
	}
	private String checkForMultiLineCommentEnd(String text){
		if(text != null){
			//delete text between '-- ' and '\n' (except last line if there's no \n)
			StringBuffer strBuf = new StringBuffer(text);
			int pos1=0;

			pos1 = strBuf.indexOf("*/"); // looking for comment end'*/' 
				
			if (pos1==-1){
				return ""; // no comment end, return empty line (deleting comment content)
			}
			else{ // comment end found

				multiLineComment = false;

				strBuf.replace(0, pos1+2, ""); // deleting the comment content from start to '*/'
					
				// check if another comment starts in same line
				text  = checkForMultiLineCommentStart(strBuf.toString());
				text = removeOneLineComment(text);
			}
		}
		return text;
	}
	private String checkForMultiLineCommentStart(String text){
		if(text != null){
			StringBuffer strBuf = new StringBuffer(text);
			int pos1=0;
			int pos2=0;
			boolean run = true;
			while(run){
				pos1 = strBuf.indexOf("/*"); // looking for '/*' 
				
				if (pos1!=-1){ // multiline comment found
					pos2 = strBuf.indexOf("*/", pos1); // looking for comment end '*/' 
					if (pos2==-1) {
						pos2 = strBuf.length(); // no comment end found, end of line instead
						multiLineComment = true;					
						run = false;
					}
					else // comment end found
						pos2 += 2; // set position after comment end
					strBuf.replace(pos1, pos2, ""); // deleting the comment
				}
				else
					run = false;
			}
			text = strBuf.toString();
		}
		return text;
	}
	private String removeOneLineComment(String text){
		if(text != null){
			//delete text between '-- ' and '\n' (except last line if there's no \n)
			StringBuffer strBuf = new StringBuffer(text);
			int pos1=0;

				int p1 = strBuf.indexOf("-- "); // looking for '-- ' 
				int p2 = strBuf.indexOf("#"); // looking for '#'
				
				if (p1 == -1 && p2 == -1)
					return text;
				if (p1 < p2 || p2 == -1){
					pos1 = p1;
					if(pos1 == -1)
						pos1 = p2;
				}
				
				if (pos1!=-1)
					strBuf.replace(pos1, strBuf.length(), ""); // deleting the text between the delimiter

				text = strBuf.toString();
		}
		return text;
	}
}
