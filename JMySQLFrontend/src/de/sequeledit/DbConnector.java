package de.sequeledit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import ds.swingutils.MessageBox;

/**
 * This is the database modul that acts as interface for
 * new sql modules. If you want to implement a new sql
 * modul for a different sql driver/dbms, then you have
 * to inherit your new class DbModulNewDb() from this one.
 * Override the nessecary methods to add functionality.
 * @author MiFri
 *
 */
abstract public class DbConnector {

	public Connection _connection = null;
	protected String _connectionString = null;
	protected Vector<Vector<Vector<Object>>> _result = new Vector<Vector<Vector<Object>>>();
	protected int _columnsNumber;
	protected String _tableName;
	protected String _sqlState;
	protected boolean _updateStatement = false;
	protected boolean _dataTypeIsImg = false;
	protected String _lastQueryStmt = "";

	/**
	 * Try to close current connection;
	 */
	public void closeConnection() {
		try {
			if (_connection != null) {
				_connection.close();
				_columnsNumber = 0;
				if(_result != null){
				_result.clear();
				_result = null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	} 

	/**
	 * This method has to return list of databases (database names as String) from the current connection. 
	 * @return List of Strings containing all database names
	 * @throws SQLException
	 */
	abstract public List<String> getListOfDatabases() throws SQLException;	

	/**
	 * This method has to returns all tables in specified database as ArrayList of Strings.
	 * @param datenbank - database which contains tables 
	 * @return List of Strings containing all table names
	 * @throws SQLException
	 */
	abstract public List<String> getListOfTables(String datenbank) throws SQLException;

	/**
	 * This method has to returns column names of specified table in specified database as 
	 * ArrayList of Strings.
	 * @param datenbank - database which contains table
	 * @param tabelle - table to retrieve column names from
	 * @return List of Strings containing all column names
	 * @throws SQLException
	 */
	abstract public List<String> getListOfColumns(String datenbank, String tabelle) throws SQLException;

	/**
	 * deprecated 
	 * This method has to return all fields for a given column in a table. 
	 * @param tabelle is the table for which the data will be returned
	 * @param spalte is the column of the table, only the data for this column will be returned 
	 * @return the result data for the specified column of the table as vector of Strings. 
	 */
	abstract public Vector<Object> getColumnData(String tabelle, String spalte, int limit) throws SQLException;

	abstract public Vector<Vector<Vector<Object>>> getTableData(String db, String tabelle, int limit, String orderType) throws SQLException;
	//abstract public boolean getTableData2(DefaultTableModel dbTableModel, String db, String tabelle, int limit, BinaryRenderer bn) throws SQLException;
	/**
	 * Set the current database
	 * @param datenbank database to be set
	 * @throws SQLException
	 */
	public void setDatabase(String datenbank) throws SQLException{
		this._connection.setCatalog(datenbank);
	}
	
	/**
	 * Get the current database name.
	 * @return the current catalog name or null
	 * @throws SQLException
	 */
	public String getDatabase() throws SQLException{
		//System.out.println("DB: " + this._connection.getCatalog());
		return this._connection.getCatalog();
	}

	/**
	 * Get the number of columns of currently active table.
	 * @return the number of columns for active table
	 */
	public int getColumnsNumber(){
		return _columnsNumber;
	}
	
	/**
	 * Get the name of the currently active table.
	 * @return the name of the active table
	 */
	public String getTableName(){
		return _tableName;
	}
	
	/**
	 * Set the currently active table.
	 * @param name the name for the table
	 */
	public void setTableName(String name){
		_tableName = name;
	}
	
	/**
	 * Get the state message from the last executed sql statement (setSqlStatement()).
	 * @return the status message from sql driver as string
	 */
	public String getSqlState(){
		return _sqlState;
	}

	/**
	 * Determine if the last sql statement was an update statement (like 'insert into' for example)
	 * @return true if statement is update statement otherwise false
	 */
	public boolean getIsUpdateStmt(){
		return _updateStatement;
	}
	
	/* deprecated */
	protected void setColumnTypeIsImg(boolean dataTypeImg){
		_dataTypeIsImg = dataTypeImg;
	}
	
	/* deprecated */
	public boolean getColumnTypeIsImg(){
		return _dataTypeIsImg;
	}
	
	/**
	 * Checks if a byte array contains an image (jpeg, png, gif). This helper method
	 * is useful to determine if a blob data type is an image. If you get the table 
	 * data, use this method as follow: 1) get data from database: byte bytearr[] = 
	 * ResultSet.getBytes(); 2) check data: checkIfBlobIsImage(bytearr). It returns 
	 * true if the byte array start with typical image bytes.
	 * @param bytearr is the data (sql blob type) to be checked
	 * @return true if the byte array contains an image, otherwise false
	 */
	protected boolean checkIfBlobIsImage(byte bytearr[]){
		StringBuffer sb = new StringBuffer();
		byte abyte0[];
        long count = 0L;
        boolean result = false;
        int l = (abyte0 = bytearr).length;
        for (int k = 0; k < l; k++)
        {
        	byte b = abyte0[k];
        	if (count == 8L)
        		break;
        	sb.append(String.format("%02X ", b));
        	count++;
        }
        if(sb.toString().startsWith("FF D8 FF")) {
            //System.out.println(" is JPG ");
            result = true;
        }
        else if(sb.toString().startsWith("47 49 46 38 37 61") || sb.toString().startsWith("47 49 46 38 39 61")){
            //System.out.println(" is GIF ");
            result = true;
        }
        else if(sb.toString().startsWith("89 50 4E 47 0D 0A 1A 0A")){
            //System.out.println(" is PNG ");
            result = true;
        }
        setColumnTypeIsImg(result);
        return result;
	}
	
	/**
	 * Call this method if the current statement is an update statement. It tries to determine
	 * the correct table on which the statement was executed and sets that table as active table. 
	 * @param statement the statement to be checked for valid table 
	 */
	protected void setUpdateStmt(String statement){
		_updateStatement = true;
		try {
			setTableName(getFirstTableNameFromSqlStmt(getDatabase(), statement));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This method determines in an arbitrary sql statement the first valid table name if any. Valid means 
	 * that the table exists in currently active database. This helper method is a little hack to determine
	 * the table on which an update statement was executed.
	 * @param database database from which tables will be used to check for valid table in statement 
	 * @param statement statement to check for valid table name
	 * @return the valid table name or null
	 */
	protected String getFirstTableNameFromSqlStmt(String database, String statement){
		String result = "";
		// 1. Tabellenname aus Update-Statement ermitteln (falls vorhanden) 
		String stringToCheck = statement.trim(); 
		stringToCheck = stringToCheck.replace('(', ' '); // Hinter Tabellenname kann direkt "(" kommen nach SQL Syntax
		int posSpace = 0;
		while(posSpace != -1){
			stringToCheck = stringToCheck.trim(); // ggf. Leerzeichen am Anfang entfernen  " aaaa bbbb cccc " -> "aaaa bbbb cccc"
			posSpace = stringToCheck.indexOf(" "); // suche nach naechstem Leerzeichen (als neues Stringende) "aaaa_bbbb cccc" = 4
			if(posSpace < 0)
				break;
			String separatedWord = stringToCheck.substring(0, posSpace); //teile String an Leerzeichen temp2 = "aaaa"
			stringToCheck = stringToCheck.substring(posSpace); // "aaaa_bbbb cccc" -> "bbbb cccc"
			try{
				List<String> listeTabs = getListOfTables(getDatabase());
				//System.out.println("Micha: " + separatedWord);

				for (String elementTab : listeTabs) {
					if(separatedWord.equals(elementTab)){
						//System.out.println("Treffer: " + separatedWord);
						result = elementTab;
						posSpace = -1;
					}			    		
				}	
			}catch(SQLException ex){
				ex.printStackTrace();
			}
			finally{

			}
		}
		return result;
	}
	
	/**
	 * This method has to return the description table for a given table (for example mysql -> desc [table]). 
	 * @param tableName specifies the name of the table for which the description table will be returned
	 * @return the description table as 3 dimensional vector
	 */
	abstract public Vector<Vector<Vector<Object>>> getTableDescription(String tableName) throws SQLException;
	
	/**
	 * This method has to execute sql statement. If the statement returns result set (is query), 
	 * the resulting table has to be represented in a 3 dimensional Vector of 
	 * the following form:
	 * result[0][0][0...n]: all column names.	
	 * result[0][1][0...n]: all column types.
	 * result[1][0...n (row)][0...n (field)]: each row as Vector of Objects.
	 * @param statement - sql statement to be executed
	 * @param showState - if true, status message for executed statement will be generated, use getSqlState() afterwards.
	 * @return - 3 dimensional Vector of Objects containing table data | null.
	 * @throws SQLException
	 */		
	abstract public Vector<Vector<Vector<Object>>> setSqlStatement(String statement, boolean showState) throws SQLException;
	//abstract public boolean setSqlStatement2(DefaultTableModel dbTableModel, String statement, boolean showState, BinaryRenderer bn) throws SQLException;
}
