/**
  * A modal dialog, giving the user chance to set the path to the MySQL-server, the username and the password
  * The response is needed for the login.
  * 
  * @author M.Friedrich, D. Scholz
  * @version 1.0
  * 
  */

package de.sequeledit;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

//Enum definieren
enum db { MYSQL, SQLITE, MSSQL };

public class LoginDatenAbfrage {
	private String _path;			// path to the server without any prefixes or endings. 
									// "localhost" not "http://localohost/" 
	private String _user;
	private String _password;
	private db _dbserver; 
	public JTextField fieldPathSqLite;

	public LoginDatenAbfrage() {
	}
		
	public String getPath() {
		return _path;
	}

	public void setPath(String path) {
		this._path = path;
	}

	public String getUser() {
		return _user;
	}

	public void setUser(String user) {
		this._user = user;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		this._password = password;
	}
	public db getDbServer(){
		return _dbserver;
	}
	public void setDbServer(db dbserver){
		this._dbserver = dbserver;
	}
	

	/**
	 * This method asks for the path to the MySQL-server, the name of the user and the password 
	 * to cennct to a MySQL-server. 
	 * 
	 * @param prePfad		path, given by the program (maybe later used by loaded user settings)
	 * @param preUser   	MySQL-user (given by the program (maybe later used by loaded user settings)
	 * @param prePassword   MySQL-password (set by the program just for testing purposes)
	 * 
	 * @return Boolean   	true: all text-field have been filled
	 * 	       			 	false: canceled
	 */
	public Boolean askForLoginData(Component parent, String prePath, String preUser, String prePassword, boolean preIsSqLiteDb) {
		
		int option = 1;								// value get by the JOptionPane-dialog
		boolean checkInput = false; 					
		//checking path i.e. ("//\\.loc alho st.//\\");
		
		// 
		while (option == 1 && !checkInput) {
			JPanel MySql = new JPanel();
			JPanel SqLite = new JPanel();
			JPanel MsSql = new JPanel();
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.addTab("MariaDB", null, MySql,"MySQL- oder MariaDB-Datenbankverbindung");
			tabbedPane.addTab("SQLite",null, SqLite,"SQLite-Datenbankverbindung");
			tabbedPane.addTab("MsSql",null, MsSql,"MS SQL-Server-Datenbankverbindung");
			//tabbedPane.setLayout(new BoxLayout(tabbedPane, BoxLayout.PAGE_AXIS));
			MySql.setLayout(new BoxLayout(MySql, BoxLayout.PAGE_AXIS));
			SqLite.setLayout(new BoxLayout(SqLite, BoxLayout.PAGE_AXIS));
			JTextField mysqlfieldPath = new JTextField(25);
			JTextField mysqlfieldUser = new JTextField(25);
			JPasswordField mysqlfieldPassword = new JPasswordField(20);
			fieldPathSqLite = new JTextField(25);
			fieldPathSqLite.setMaximumSize(new Dimension(1000, 19));

		
			// filling the text-fields with given values
			if (prePath != null) { mysqlfieldPath = new JTextField(prePath);}
			if (preUser != null) { mysqlfieldUser = new JTextField(preUser); }
			if (prePassword != null) { mysqlfieldPassword = new JPasswordField(prePassword); }
			//checkBox.setSelected(preIsSqLiteDb);

			mysqlfieldPath.setToolTipText("Bitte hier den Pfad zum MySQL-Server angeben.\n\n Beispiele:\n");
			fieldPathSqLite.setToolTipText("Bitte hier den Pfad zur SQLite-Datenbank (neu/bestehend) angeben\n\n oder leer lassen für virtuelle Datenbank\n");
			// sequence is important for the layout
			MySql.add(new JLabel("Pfad:"));
			MySql.add(mysqlfieldPath);
			MySql.add(new JLabel("Benutzer:"));
			MySql.add(mysqlfieldUser);
			MySql.add(new JLabel("Passwort:"));
			MySql.add(mysqlfieldPassword);
			//MySql.add(new JLabel("SqLite Datenbank:"));
			//MySql.add(checkBox);
			JButton btn = new JButton("Öffnen/Erstellen");
			SqLite.add(new JLabel("Pfad:"));
			SqLite.add(fieldPathSqLite);
			SqLite.add(btn);
			btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setDialogTitle("Öffne bestehende oder erstelle neue SQLite-Datenbank");
					fileChooser.setFileFilter(new FileNameExtensionFilter("SQLite Datenbanken", "sqlite"));
					int returnVal = fileChooser.showOpenDialog(null);
			        if (returnVal == JFileChooser.APPROVE_OPTION){
			        	File file = fileChooser.getSelectedFile();
			        	fieldPathSqLite.setText(file.getAbsolutePath());
			        }
				}
			});
			//SqLite.add(fieldPathSqLite);
			MsSql.setLayout(new BoxLayout(MsSql, BoxLayout.PAGE_AXIS));

			JTextField mssqlfieldPath = new JTextField(25);
			JTextField mssqlfieldUser = new JTextField(25);
			JPasswordField mssqlfieldPassword = new JPasswordField(20);

			mssqlfieldPath.setToolTipText("Bitte hier den Pfad zum MS SQL-Server angeben.\n\n Beispiele:\n");
		
			// filling the text-fields with given values
			if (prePath != null) { mssqlfieldPath = new JTextField(prePath);}
			if (preUser != null) { mssqlfieldUser = new JTextField(preUser); }
			if (prePassword != null) { mssqlfieldPassword = new JPasswordField(prePassword); }
			//checkBox.setSelected(preIsSqLiteDb);
			// sequence is important for the layout
			MsSql.add(new JLabel("Pfad:"));
			MsSql.add(mssqlfieldPath);
			MsSql.add(new JLabel("Benutzer:"));
			MsSql.add(mssqlfieldUser);
			MsSql.add(new JLabel("Passwort:"));
			MsSql.add(mssqlfieldPassword);

			String[] options = new String[]{"Abbruch", "OK"};
			option = JOptionPane.showOptionDialog(parent, tabbedPane, "Datanbankverbindung herstellen", JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
			if(option == 1) {
				// Ok gedr�ckt
				if(tabbedPane.getSelectedIndex()== 0){
					mysqlfieldPath.setText(checkPath(mysqlfieldPath.getText()));
					if ((!mysqlfieldPath.getText().equals("")) && (!mysqlfieldUser.getText().equals("")) /*&& (!pw.equals(""))*/ ) {
						setPath(mysqlfieldPath.getText());
						setUser(mysqlfieldUser.getText());
						setPassword(new String(mysqlfieldPassword.getPassword()));
						setDbServer(db.MYSQL);
						return true;
					}
				}
				else if (tabbedPane.getSelectedIndex()== 1){
					setPath(fieldPathSqLite.getText()/* + "/"*/);
					setUser("SQLite");
					setDbServer(db.SQLITE);
					return true;
				}
				else{
					mssqlfieldPath.setText(checkPath(mssqlfieldPath.getText()));
					if ((!mssqlfieldPath.getText().equals("")) && (!mssqlfieldUser.getText().equals("")) /*&& (!pw.equals(""))*/ ) {
						setPath(mssqlfieldPath.getText());
						setUser(mssqlfieldUser.getText());
						setPassword(new String(mssqlfieldPassword.getPassword()));
						setDbServer(db.MSSQL);
						return true;
					}
				}
			} else if (option == 0){
				// Abbruch
				setPath("");
				setUser("");
				setPassword("");
				return false;
			}
		}
		return false;
	}
	
	private String checkPath(String path){
		// Path ... 
		// ...may not contain a space at the beginning or at the end-> Trim 
		String result = path.trim();
		
		// ... may not contain spaces at all 	
		result = path.replace(" ", "");

		//... may not end with following characters  '/', '\', " ", "." 
		boolean test = false;
		while (!test) {
			switch (result.substring(result.length()-1)) {
				case "/":
				case "\\":
				case ".":
				case ":":
					result = result.substring(0,result.length()-1);
					test = false;
					break;
				default:
					test = true;
					break;
			}
		}
		

		//... may not start with following characters  '/', '\', " ", "."
		test = false;
		while (!test) {
			switch (result.substring(0,1)) {
				case "/":
				case "\\":
				case ".":
				case ":":					
					result = result.substring(1);
					test = false;
					break;
				default:
					test = true;
					break;
			}
		}

		if ((result.length()>=4) && (result.toUpperCase().substring(0,4).equals("HTTP"))) {
			// checking again: leading/ending '/' or '\' prüfen

			result = result.substring(4);
			test = false;
			while (!test) {
				switch (result.substring(0,1)) {
					case "/":
					case "\\":
					case ".":
					case ":":					
						result = result.substring(1);
						test = false;
						break;
					default:
						test = true;
						break;
				}
			}
		}
		return result;
	}	
}
