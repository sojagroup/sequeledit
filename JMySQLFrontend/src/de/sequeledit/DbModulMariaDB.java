package de.sequeledit;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

import ds.swingutils.MessageBox;

/**
 * This class is the MariaDB modul for communication via
 * mariadb-java-client-1.4.3.jar driver with MariaDB/MySQL databases.
 * It extends from DbConnector class. 
 * @author DiTo, MiFri
 *
 */
public class DbModulMariaDB extends DbConnector {
	//public Connection _connection = null;
	//private Vector<Vector<String>> _result = new Vector<Vector<String>>();
	//private int _columnsNumber;
	//private String _tableName;
	//private String _sqlState;
	//private boolean _updateStatement = false;

	
	/**
	 * Constructor	
	 * @param userName user name for auhentification
	 * @param pw password for auhentification
	 * @param pfad path for connection
	 */
	public DbModulMariaDB (String userName, String pw, String pfad) {
		if (!connectToDb(userName, pw, pfad, null)) {
			// keine verbindung!
		}
	}
	
	/**
	 * Constructor
	 * @param userName user name for auhentification
	 * @param pw password for authentification
	 * @param pfad path for connection
	 * @param datenbank database
	 */
	public DbModulMariaDB (String userName, String pw, String pfad, String datenbank) {
		if (!connectToDb(userName, pw, pfad, datenbank)) {
			// keine verbindung!
		}
	}
	
	/**
	 * Try to connect to MariaDB/MySQL database. This method takes user name, password,
	 * path and optional database for authentification and connection to database server.
	 * @param userName user name
	 * @param pw password, may be empty if no password is needed
	 * @param pfad url to database server
	 * @param datenbank database to connect to, can be null
	 * @return true if connection to database is successful otherwise false
	 */
	private  Boolean connectToDb(String userName, String pw, String pfad, String datenbank) {
		try {

			// Laden des MySQL-Treibers
			Class.forName("org.mariadb.jdbc.Driver");
			//Class.forName("com.mysql.jdbc.Driver");
			
			// Herstellen der Verbindung
			String url = "";
			if (datenbank !=null)  
				//url = "jdbc:mysql://localhost/" + datenbank + "?user=" + userName + "&password=" + pw;
				_connectionString = url = "jdbc:mysql://" + pfad + "/" + datenbank + "?user=" + userName + "&password=" + pw;
			else
				//url = "jdbc:mysql://localhost/?user="+ userName + "&password=" +pw;
				_connectionString = url = "jdbc:mysql://" + pfad + "/" + "?user="+ userName + "&password=" +pw;
			
			_connection = DriverManager.getConnection(url);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			MessageBox meinPopUp = new MessageBox();
			meinPopUp.showMessage("Der JDBC-Treiber kann nicht geladen werden!");
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			MessageBox meinPopUp = new MessageBox();
			meinPopUp.showMessage("Es kann keine Verbindung für den Benutzer "+ userName + " hergestellt werden!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * Check if connection to db is valid and try reconnect with current connection string if not. 
	 * @throws SQLException
	 */
	private void checkAndReconnect() throws SQLException{
		if(!_connection.isValid(0)){
			//System.out.println("Try reconnect...");
			_connection = DriverManager.getConnection(_connectionString);
		}
	}
	/**
	 * Returns list of databases (database names as String) from the current connection. 
	 * @return List of Strings containing all database names
	 * @throws SQLException
	 */
	public List<String> getListOfDatabases() throws SQLException {
		checkAndReconnect();
		List<String> result = new ArrayList<String>();
		
		ResultSet recSet = _connection.getMetaData().getCatalogs();
		while (recSet.next()) {
			result.add(recSet.getString("TABLE_CAT"));
		}
		recSet.close();
		return result ;
	}

	/**
	 * Returns all tables in specified database as ArrayList of Strings.
	 * @param datenbank - database which contains tables 
	 * @return List of Strings containing all table names
	 * @throws SQLException
	 */
	public List<String> getListOfTables(String datenbank) throws SQLException {
		checkAndReconnect();
		List<String> result = new ArrayList<String>();
		//String[] types = {"TABLE"};
		ResultSet recSet = _connection.getMetaData().getTables(datenbank, null, "%", null);

		// java.sql.Statement stmt = _connection.createStatement();
		// ResultSet recSet = stmt.executeQuery("Select table_name FROM user_tables");
		while (recSet.next()) {
			  //result.add(recSet.getString(3));
			  result.add(recSet.getString("TABLE_NAME"));
		}
		recSet.close();
		return result ;
	}
		
	/**
	 * Returns column names of specified table in specified database as ArrayList of Strings.
	 * @param datenbank - database which contains table
	 * @param tabelle - table to retrieve column names from
	 * @return List of Strings containing all column names
	 * @throws SQLException
	 */
	public List<String> getListOfColumns(String datenbank, String tabelle) throws SQLException {
		checkAndReconnect();
		List<String> result = new ArrayList<String>();
		
		ResultSet resSet = this._connection.getMetaData().getColumns(datenbank, null, tabelle, null);
		while (resSet.next()) {
			result.add(resSet.getString("COLUMN_NAME"));
		}
		this._connection.setCatalog(datenbank);
		resSet.close();
		return result ;
	}
	
	private ArrayList<String> parseShowCreateTableResult(String result, String orderType){
		String[] lines = result.split("[\\r\\n]+"); // split new lines and empty ones
		ArrayList<ArrayList<String>> tables = new ArrayList<ArrayList<String>>();
		int count = 0;
		String columnString = " ";
		String columnStringClean = " ";
		String orderByPKString = "";
		for(String line : lines){
			line = line.trim();
			/*if (!line.startsWith("CREATE TABLE") && !line.startsWith("PRIMARY KEY")
					&& !line.startsWith("KEY") && !line.startsWith("CONSTRAINT")
					&& !line.startsWith(")")){*/
			if (line.startsWith("`")){
				tables.add(new ArrayList<String>());
				String tableName = line.substring(0, line.lastIndexOf("`")+1);
				line = line.substring(line.lastIndexOf("`")+1).trim();
				//System.out.println(line);
				String[] tokens = line.split("[ ]+"); // split on spaces
				tables.get(count).add(tableName);
				for(int x = 0; x < tokens.length; x++){
					tables.get(count).add(tokens[x].replace(",", ""));
				}
				count++;
			}
			else if (line.startsWith("PRIMARY KEY")){
				orderByPKString = line.substring(line.indexOf("`"), line.lastIndexOf("`")+1);
			}
		}
		ArrayList<String> parsedResults = new ArrayList<String>();
		for(ArrayList<String> row : tables){
			String colName = row.get(0); // columntype
			String colType = row.get(1); // columntype

			if (colType.startsWith("varchar(") || colType.startsWith("char(")){	
				int value = Integer.parseInt(row.get(1).replaceAll("[^0-9]", ""));
				if(value > 256){	
					columnString += " LEFT(" + colName + ", 256) AS " + colName +", ";
					columnStringClean += " LEFT(" + colName + ", 256), ";
				}
				else{
					columnString += colName + ", ";
					columnStringClean += colName + ", ";
				}
			}
			else if (colType.startsWith("longblob") || colType.startsWith("blob") ||
					colType.startsWith("tinyblob") || colType.startsWith("mediumblob") ||
					colType.startsWith("text") || colType.startsWith("binary")){		
				columnString += " LEFT(" + colName + ", 256) AS " + colName +", ";
				columnStringClean += " LEFT(" + colName + ", 256), ";
			}
			else{
				columnString += colName + ", ";
				columnStringClean += colName + ", ";
			}
		}
		
		parsedResults.add(columnString.substring(0, columnString.lastIndexOf(",")).trim());
		parsedResults.add(columnStringClean.substring(0, columnStringClean.lastIndexOf(",")).trim());
		if(!orderType.isEmpty() && orderByPKString.length() > 0){
			parsedResults.add(" ORDER BY " + orderByPKString + " " + orderType);
		}
		else{
			parsedResults.add(""); // add empty
		}
		//System.out.println(tableNames);
		return parsedResults;
	}
	
	/**
	 * Returns complete table with column names, column types, and rows. 
	 * @param db is the database for which the data will be returned
	 * @param tabelle is the table for which the data will be returned
	 * @param spalte is the column of the table, only the data for this column will be returned 
	 * @return the result data for the table as vector of vector of vector of objects. 
	 */
	public Vector<Vector<Vector<Object>>> getTableData(String db, String tabelle, int limit, String orderType) throws SQLException {
		// check if connected else reconnect
		checkAndReconnect();
		
		// set database
		this._connection.setCatalog(db);
		
		String sqlShowCreate = "SHOW CREATE TABLE " + "`"+tabelle+"`";
		
		System.out.println(sqlShowCreate);
		Vector<Vector<Vector<Object>>> vShowCreate = setSqlStatement(sqlShowCreate, true);
		
		String sShowCreate = vShowCreate.get(1).get(0).get(1).toString();
		Object type = vShowCreate.get(0).get(0).get(0);
		_tableName = vShowCreate.get(1).get(0).get(0).toString();
		// returns ArrayList with 3 Strings
		// ArrayList[0]: coulumn names 
		// ArrayList[1]: coulumn names cleaned
		// ArrayList[2]: order by clause
		ArrayList<String> colParsed = ((type.equals("Table"))?parseShowCreateTableResult(sShowCreate, orderType):null);
		// define SQL Stmnt
		String orderByClause = ((colParsed != null)?colParsed.get(2):"");
		String sqlStatement = "SELECT " + ((colParsed != null)?colParsed.get(0):"*") + " FROM " + "`"+tabelle+"`" + orderByClause;
		System.out.println("SELECT " + ((colParsed != null)?colParsed.get(1):"*") + " FROM " + "`"+tabelle+"`" + orderByClause);
		// prepare row limit string if any
		String rowLimit = (limit > 0)?" LIMIT " + String.valueOf(limit):"";
		
		return setSqlStatement(sqlStatement + rowLimit, true);
	}
	
	/*public boolean getTableData2(DefaultTableModel dbTableModel, String db, String tabelle, int limit) throws SQLException {
		// check if connected else reconnect
		checkAndReconnect();
		
		// set database
		this._connection.setCatalog(db);

		// define SQL Stmnt
		String sqlStatement = "SELECT * FROM " + "`"+tabelle+"`";
		
		// prepare row limit string if any
		String rowLimit = (limit > 0)?" LIMIT " + String.valueOf(limit):"";
		
		return setSqlStatement2(dbTableModel, sqlStatement + rowLimit, true);
	}*/
	
	/**
	 * Returns all fields for a given column in a table. 
	 * @param tabelle is the table for which the data will be returned
	 * @param spalte is the column of the table, only the data for this column will be returned 
	 * @return the result data for the specified column of the table as vector of Strings. 
	 */
	public Vector<Object> getColumnData(String tabelle, String spalte, int limit) throws SQLException {
		checkAndReconnect();
		_tableName = tabelle;
		Vector<Object> result = new Vector<Object>();
		result.clear();
		
		java.sql.Statement stmt = this._connection.createStatement();
		ResultSet resSet;

		//resSet = stmt.executeQuery("SELECT " + spalte + " FROM " + tabelle); // buggy: data will be sorted
		// using back quotes for table names with spaces
		String rowLimit = (limit > 0)?" LIMIT " + String.valueOf(limit):"";
		resSet = stmt.executeQuery("SELECT * FROM " + "`"+tabelle+"`" + rowLimit);
		java.sql.ResultSetMetaData rsmd=resSet.getMetaData();

		System.out.println("SELECT * FROM " + "`"+tabelle+"`" + rowLimit);
		while (resSet.next()) {
			switch(rsmd.getColumnTypeName(1)){
			case "TINYBLOB":
			case "BLOB":
			case "MEDIUMBLOB":
			case "LONGBLOB":
				byte bytearr[] = resSet.getBytes(spalte);
				if(checkIfBlobIsImage(bytearr)){
					ImageIcon img =  new ImageIcon(bytearr);
					result.add(img);
					setColumnTypeIsImg(true);
				}
				else{
			    	//Object field = resSet.getObject(spalte);
			    	result.add(bytearr);
					setColumnTypeIsImg(false);
				}
				break;
			default:
		    	Object field = resSet.getObject(spalte);
		    	//System.out.println(resSet.getString(spalte));
		    	result.add(field);
				setColumnTypeIsImg(false);
			}
					
		}
		resSet.close();
		return result;
	}
	
	/**
	 * Returns the description table for a given table. 
	 * @param tableName specifies the name of the table for which the description table will be returned
	 * @return the description table as 2 dimensional vector
	 */
	public Vector<Vector<Vector<Object>>> getTableDescription(String tableName) throws SQLException {
		checkAndReconnect();
		// using back quotes for table names with spaces
		return setSqlStatement("DESC `" + tableName +"`", false);
	}
	
	/**
	 * Executes sql statement. If the statement returns result set (is query), 
	 * the resulting table has to be represented in a 3 dimensional Vector of 
	 * the following form:
	 * result[0][0][0...n]: all column names.	
	 * result[0][1][0...n]: all column types.
	 * result[1][0...n (row)][0...n (field)]: each row as Vector of Objects.
	 * @param statement - sql statement to be executed
	 * @param showState - if true, status message for executed statement will be generated, use getSqlState() afterwards.
	 * @return - 3 dimensional Vector of Objects containing table data | null.
	 * @throws SQLException
	 */
	
	// w0151e17.kasserver.com
	// d0243d14
	// AG2GzLbaK2aZJh48
	//
	public Vector<Vector<Vector<Object>>> setSqlStatement(String statement, boolean showState) throws SQLException {
		// check if connected else reconnect
		checkAndReconnect();
		
		// initialize update stmnt to false
		_updateStatement = false;
		
		// initialize execution timer
		long startTime = System.nanoTime();
		long elapsedTime =0;
	    java.text.NumberFormat f = java.text.NumberFormat.getInstance();
	    f.setMinimumFractionDigits(  2 );  
	    f.setMaximumFractionDigits(  2 );  
	    
	    // create Vector for result
	    _result = new Vector<Vector<Vector<Object>>>();
		//java.sql.Statement stmt = this._connection.createStatement();
		java.sql.PreparedStatement stmt = this._connection.prepareStatement(statement, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
		stmt.setFetchSize(10);
		//stmt.setMaxFieldSize(256); // useless???
		


		// reset last Query Statement
		_lastQueryStmt = "";
		
		try{
			// if stmnt has result set...
			if(stmt.execute()){
				//System.out.println(statement);
				ResultSet resSet;
				resSet = stmt.getResultSet();
				java.sql.ResultSetMetaData rsmd=resSet.getMetaData();
				_columnsNumber = rsmd.getColumnCount();
				// set last Query Statement
				_lastQueryStmt = statement;
				if(_columnsNumber == 0)
					return _result;
				else{
					if(!statement.contains("DESC "))
						_tableName = rsmd.getTableName(1);
					
					// add new meta data vector [0]
					_result.add(new Vector<Vector<Object>>());
					// add new col names vector [0][0]
					_result.get(0).add(new Vector<Object>());
					// add new col types vector [0][1]
					_result.get(0).add(new Vector<Object>());
					
					// get column names and types
					for(int x = 1; x <= _columnsNumber; x++){
						_result.get(0).get(0).add(rsmd.getColumnName(x));
						_result.get(0).get(1).add(rsmd.getColumnTypeName(x));
					}
					
					// create new table data vector [1]
					_result.add(new Vector<Vector<Object>>());
					
					// iterate through rows in result set
					int row = 0;
					while (resSet.next()) {
						// add new row vector [1][row]
						_result.get(1).add(row, new Vector<Object>());

						for(int col=1; col <= _columnsNumber; col++){
							_result.get(1).get(row).add(resSet.getObject(col));
						}
						row++;				
					}
				
					elapsedTime = System.nanoTime() - startTime;
					//resSet.last();
					int size = row;//resSet.getRow();
					//resSet.beforeFirst();
					if(showState){
						if(size == 0){
							_sqlState = "Empty set (" + f.format(elapsedTime/1000000000.0) + " sec)";
						}
						else if(size == 1){
							_sqlState = size + " row in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}
						else{
							_sqlState = size + " rows in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}	
					}
				}
				resSet.close();
			}
			else{
				if(statement.contains("USE ")){
					setDatabase(statement.substring(statement.indexOf(" "), statement.length()-1).trim());
				}
			
				//_updateStatement = true;
				int rows = stmt.getUpdateCount();
			
				setUpdateStmt(statement);
			
				if(showState){
					if(rows == 1){
						_sqlState = "Query OK, " + rows + " row affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
					else{
						_sqlState = "Query OK, " + rows + " rows affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
				}
			}
		}
		catch(SQLException ex){
			// handle any errors
			if(showState)
				_sqlState = "Error " + ex.getErrorCode() + " (" + ex.getSQLState() + "): " + ex.getMessage() + " " + SQLError.mysqlToXOpen(ex.getErrorCode()) + " : " +SQLError.get(SQLError.mysqlToXOpen(ex.getErrorCode()));
		}
		finally{
	
		}
		return _result;
	}
	/*public boolean setSqlStatement2(DefaultTableModel dbTableModel, String statement, boolean showState, BinaryRenderer bn) throws SQLException {
		// check if connected else reconnect
		checkAndReconnect();
		
		// initialize update stmnt to false
		_updateStatement = false;
		
		boolean result = false;
		
		// initialize execution timer
		long startTime = System.nanoTime();
		long elapsedTime =0;
	    java.text.NumberFormat f = java.text.NumberFormat.getInstance();
	    f.setMinimumFractionDigits(  2 );  
	    f.setMaximumFractionDigits(  2 );  
	    
	    // create Vector for result
	    _result = new Vector<Vector<Vector<Object>>>();
		//java.sql.Statement stmt = this._connection.createStatement();
		java.sql.PreparedStatement stmt = this._connection.prepareStatement(statement, java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
		stmt.setFetchSize(10);
		
		//stmt.setMaxFieldSize(0);

		// reset last Query Statement
		_lastQueryStmt = "";
		
		try{
			// if stmnt has result set...
			if(stmt.execute()){
				//System.out.println(statement);
				ResultSet resSet;
				resSet = stmt.getResultSet();
				java.sql.ResultSetMetaData rsmd=resSet.getMetaData();
				_columnsNumber = rsmd.getColumnCount();
				// set last Query Statement
				_lastQueryStmt = statement;
				if(_columnsNumber == 0)
					return result;
				else{
					if(!statement.contains("DESC "))
						_tableName = rsmd.getTableName(1);
					
					Vector<Object> colTypes = new Vector<Object>();
					// get column names and types
					for(int x = 1; x <= _columnsNumber; x++){
						colTypes.add(rsmd.getColumnTypeName(x));

						dbTableModel.addColumn(rsmd.getColumnName(x));

					}
					//colNames.clear();
					//colTypes.clear();
					
					// iterate through rows in result set
					int row = 0;
					while (resSet.next()) {
						//Vector<Object> rowData = new Vector<Object>();
						Object [] rowData = new Object[_columnsNumber];
						for(int col=0; col < rowData.length; ++col){						
							rowData[col] = resSet.getObject(col+1);
						}
						dbTableModel.addRow(rowData);
						//rowData.clear();
						row++;				
					}
					result=true;
					elapsedTime = System.nanoTime() - startTime;
					//resSet.last();
					int size = row;//resSet.getRow();
					//resSet.beforeFirst();
					if(showState){
						if(size == 0){
							_sqlState = "Empty set (" + f.format(elapsedTime/1000000000.0) + " sec)";
						}
						else if(size == 1){
							_sqlState = size + " row in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}
						else{
							_sqlState = size + " rows in set (" + f.format(elapsedTime/1000000000.0)  + " sec)";
						}	
					}
				}
				resSet.close();
			}
			else{
				if(statement.contains("USE ")){
					setDatabase(statement.substring(statement.indexOf(" "), statement.length()-1).trim());
				}
			
				//_updateStatement = true;
				int rows = stmt.getUpdateCount();
			
				setUpdateStmt(statement);
			
				if(showState){
					if(rows == 1){
						_sqlState = "Query OK, " + rows + " row affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
					else{
						_sqlState = "Query OK, " + rows + " rows affected (" + f.format(elapsedTime/1000000000.0)  + " sec)";
					}
				}
			}
		}
		catch(SQLException ex){
			// handle any errors
			if(showState)
				_sqlState = "Error " + ex.getErrorCode() + " (" + ex.getSQLState() + "): " + ex.getMessage() + " " + SQLError.mysqlToXOpen(ex.getErrorCode()) + " : " +SQLError.get(SQLError.mysqlToXOpen(ex.getErrorCode()));
		}
		finally{
	
		}
		return result;
	}*/

}

