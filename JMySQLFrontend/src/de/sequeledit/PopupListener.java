package de.sequeledit;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;
import javax.swing.JTextPane;


public class PopupListener extends MouseAdapter {
	
	JPopupMenu popup;
	    public PopupListener(JPopupMenu p){
	    	popup = p;
	    }
	    public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	popup.show(e.getComponent(),
	                       e.getX(), e.getY());

	        }
	    }
}
