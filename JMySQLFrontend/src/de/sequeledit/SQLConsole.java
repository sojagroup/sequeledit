package de.sequeledit;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.InputMap;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

interface ConsoleListener{
    void executeConsoleStatement();
}

public class SQLConsole extends JTextArea{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Liste aller registrierten Listener
	private List<ConsoleListener> listeners = new ArrayList<ConsoleListener>();

	private String _prompt = "mysql> ";
	private String _secPmt = "    -> ";
	private String _prmpt2 = _prompt.substring(0, _prompt.length()-1);
	private String _dlimit = ";";
	private boolean _dlimitSwitch = false;
	//private String _secPt2 = _secPmt.substring(0, _secPmt.length()-1);
	private int _caretPosAtPrompt = _prompt.length(); // die Position des Caret (Cursor) am Prompt (SQL-Konsole)
	private String _sqlStatement = "";
	private boolean historyIsInUse = false;

	
	private ArrayList<String> sqlKeywords = new ArrayList<String>(Arrays.asList("create", "select", "database", "from",
			"where", "add", "all", "distinct", "update", "set", "between", "like", "and", "inner", "join", 
			"union", "left", "right", "asc", "desc", "having", "order by", "group by", "table", "alter", "index",
			"on", "insert", "into", "delete", "values", "show", "tables", "drop", "use", "decimal", "varchar",
			"date", "char", "year", "time", "binary", "int", "integer", "tinyint", "smallint", "mediumint", "real",
			"bigint", "float", "databases", "auto_increment", "primary key", "as", "having", "foreign key", "references",
			"source", "curdate", "curtime", "delimiter", "procedure", "begin", "end", "call", "status"));
	
	private ArrayList<String> _consHistory = new ArrayList<String>();
	private int _conHistIndex;
	
	Object _Left_Arrowkey_Action;
	Object _Back_Space_Action;
	
	//private MySqlVerbindung _sqlVerbindung;
	public String getDelimiter(){
		return _dlimit;
	}

	public SQLConsole(){
		
		
		setCaretColor(Color.white);
		setEditable(false);
		setLineWrap(true);
		// Die Standard-Actions fuer Links-Arrow-Key und Back-Space-Key zwischenspeichen
		_Left_Arrowkey_Action = getActionForKeyStroke(KeyStroke.getKeyStroke("LEFT"));
		_Back_Space_Action = getActionForKeyStroke(KeyStroke.getKeyStroke("BACK_SPACE"));
		
		setFont(new Font("Monospaced", Font.BOLD, 14));
		//setBackground(UIManager.getColor("Button.foreground"));
		setBackground(Color.DARK_GRAY);
		setForeground(SystemColor.textHighlightText);
		
		// Key-Bindings fuer SQL-Editor modifizieren
		//DefaultCaret caret = (DefaultCaret)_sqlEditor.getCaret();
		//caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		//this.getActionMap().get("caret-up").setEnabled(false); // deaktiviert Pfeil rauf auch f�r TextPane
		getInputMap().put(KeyStroke.getKeyStroke("UP"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("PAGE_UP"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("PAGE_DOWN"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("KP_UP"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("KP_DOWN"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("KP_LEFT"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("KP_RIGHT"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("ENTER"),"none");
		getInputMap().put(KeyStroke.getKeyStroke("HOME"),"none"); // Pos 1
		
		InputMap map = (InputMap)UIManager.get("ScrollPane.ancestorInputMap");
        map.put(KeyStroke.getKeyStroke("UP"), "none");
		
		
		/* 	DefaultCaret caret = new DefaultCaret() {
	    public void paint(Graphics g) {
	        JTextComponent comp = getComponent();
	        if (comp == null)
	            return;

	        Rectangle r = null;
	        try {
	            r = comp.modelToView(getDot());
	            if (r == null)
	                return;
	        } catch (BadLocationException e) {
	            return;
	        }
	        r.height = 30; //this value changes the caret size
	        if (isVisible())
	            g.fillRect(r.x, r.y, 10, r.height);
	    }
	};*/
 	// caret.setBlinkRate(100);
	
	/// Test
	/*
 	sqlEditor.setCaret(new DefaultCaret() {
 	   

		private static final long serialVersionUID = 1L;
		public void paint(Graphics g) {
 			JTextComponent comp = getComponent();
 			
	        if (comp == null)
	            return;
	        Rectangle r = null;
	        try {
	            r = comp.modelToView(getDot());
	            if (r == null)
	                return;
	        } catch (BadLocationException e) {
	            return;
	        }
	        r.height = 30; //this value changes the caret size
	        if (isVisible())
	            g.fillRect(r.x, r.y, 10, 20);
	    }
	});*/

		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				// Falls User mit Maus den Cursor in Prompt oder gesperrte Position der Konsole setzen will,
				// korrigieren wir die Cursor-Position

				if(getCaretPosition() < _caretPosAtPrompt && isEditable())
				setCaretPosition(_caretPosAtPrompt);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				if(getCaretPosition() < _caretPosAtPrompt && isEditable())
				setCaretPosition(_caretPosAtPrompt);
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				if(getCaretPosition() < _caretPosAtPrompt && isEditable())
				setCaretPosition(_caretPosAtPrompt);
				
			}
		});
		
		addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {

			}
		
	
			@Override 
			public void keyPressed(KeyEvent e) {
				
				// kleiner Hack damit bei Markierung mit Maus nicht vor Prompt getippt werden kann
				if(getCaretPosition() < _caretPosAtPrompt && isEditable())
				setCaretPosition(_caretPosAtPrompt);
				// TODO Auto-generated method stub
				int key = e.getKeyCode();
				if(key==KeyEvent.VK_LEFT || key==KeyEvent.VK_BACK_SPACE){
					if(getCaretPosition() < _caretPosAtPrompt+1){
						getInputMap().put(KeyStroke.getKeyStroke("LEFT"),"none");
						getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"),"none");
						//_sqlEditor.setCaretPosition(_caretPos+1);
						//_sqlEditor.setText(_sqlEditor.getText());
					}
					else{
						getInputMap().put(KeyStroke.getKeyStroke("LEFT"), _Left_Arrowkey_Action);
						getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"), _Back_Space_Action);
					}
				}
				if(key==KeyEvent.VK_UP){
					
				}
				else if(key==KeyEvent.VK_DOWN){
					
				}
				
			}
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				//System.out.println(e.getKeyChar());
				//if(sqlEditor.getCaretPosition() < _caretPos)
					//sqlEditor.setCaretPosition(_caretPos);

				int key = e.getKeyCode();
				if(key==KeyEvent.VK_UP){
					if(!_consHistory.isEmpty()){
						historyIsInUse = true;
						/*String sqlStmt = getText();
						if(sqlStmt.length()> _caretPosAtPrompt && _conHistIndex== _consHistory.size()){
							String hist = sqlStmt.substring(_caretPosAtPrompt,sqlStmt.length());
							_consHistory.set(_consHistory.size(), hist.trim());
							//_conHistIndex= _consHistory.size();
						}*/
						_conHistIndex--;
						if(_conHistIndex < 0){
							_conHistIndex =0;
						}
						setCaretPosition(_caretPosAtPrompt);
						//System.out.println("Index: " + _conHistIndex);
						String t = getText();
						t = t.substring(0, _caretPosAtPrompt);
						setText(t);
						//_sqlEditor.append(_consHistory.get(_conHistIndex));
						insert(_consHistory.get(_conHistIndex), _caretPosAtPrompt);
						//_sqlEditor.setCaretPosition(_caretPos);
					}
				}
				else if(key==KeyEvent.VK_DOWN){
					if(!_consHistory.isEmpty()&& historyIsInUse){
						_conHistIndex++;
						if(_conHistIndex > _consHistory.size()-1){
							historyIsInUse = false;
							setCaretPosition(_caretPosAtPrompt);
							String t = getText();
							t = t.substring(0, _caretPosAtPrompt);
							setText(t);
							if(_conHistIndex > _consHistory.size())
								_conHistIndex =_consHistory.size();
						}
						else{
							setCaretPosition(_caretPosAtPrompt);
							//System.out.println("Index: " + _conHistIndex);
							String t = getText();
							t = t.substring(0, _caretPosAtPrompt);
							setText(t);
							//_sqlEditor.append(_consHistory.get(_conHistIndex));
							insert(_consHistory.get(_conHistIndex), _caretPosAtPrompt);
						}
					}
				}
				if(key == KeyEvent.VK_HOME)
					setCaretPosition(_caretPosAtPrompt);
				if(key == KeyEvent.VK_ENTER)
					append("\n");
				if(key==KeyEvent.VK_SPACE || key == KeyEvent.VK_ENTER || key == KeyEvent.VK_COMMA || (e.isShiftDown() && (key == KeyEvent.VK_COMMA || key == KeyEvent.VK_8))){
					String sqlStmt = getText(); // Leerzeichen entfernen (Anfang, Ende)
					String test = sqlStmt.toLowerCase().substring(_caretPosAtPrompt-1); // nur aktuelle Zeile parsen
					String t = sqlStmt;
					for(String tmp : sqlKeywords){

						if(test.contains(" "+tmp + " ")){
							t= replaceAll(tmp, tmp.toUpperCase(), t, true);
							int cpos = getCaretPosition();
							setText(t); // setzt Caret ans Stringende
							if(key != KeyEvent.VK_ENTER) // nur wenn nicht Enter gedrueckt wurde, wird Caret-Pos ausgelesen und gesetzt (falls innerhalb der Zeile editiert wird)
								setCaretPosition(cpos);
						}					
						if(test.contains(" "+tmp + "\n")){
							t= replaceAll(tmp, tmp.toUpperCase(), t, true);
							int cpos = getCaretPosition();
							setText(t);
							if(key != KeyEvent.VK_ENTER) 
								setCaretPosition(cpos);
						}
						if(test.contains(" " + tmp + ";")){
							t= replaceAll(tmp, tmp.toUpperCase(), t, true);
							int cpos = getCaretPosition();
							setText(t);
							if(key != KeyEvent.VK_ENTER) 
								setCaretPosition(cpos);
						}
						if(test.contains(" " + tmp + "(")){
							t= replaceAll(tmp, tmp.toUpperCase(), t, true);
							int cpos = getCaretPosition();
							setText(t);
							if(key != KeyEvent.VK_ENTER) 
								setCaretPosition(cpos);
						}
						if(test.contains(" " + tmp + ",")){
							t= replaceAll(tmp, tmp.toUpperCase(), t, true);
							int cpos = getCaretPosition();
							setText(t);
							if(key != KeyEvent.VK_ENTER) 
								setCaretPosition(cpos);
						}
					}
			
					if(key == KeyEvent.VK_ENTER){
						historyIsInUse = false;
						_conHistIndex= _consHistory.size(); // History-Zaehler resetten
						sqlStmt = getText().trim(); // Leerzeichen entfernen (Anfang, Ende)
						String temp = replaceAll(_prompt, "", sqlStmt, true); // Prompt aus Statement entfernen
						temp = replaceAll(_secPmt, "", temp, true);
					/*	if(temp.contains("DELIMITER"+_dlimit)){
							// Syntax-Fehler, z.B: DELIMITER;
							//ERROR: 
							//	DELIMITER must be followed by a 'delimiter' character or string
						}
						if(temp.contains("DELIMITER ")){
							temp = replaceAll("DELIMITER ", "", temp, true);
							temp.trim();
							_dlimit = temp;
							System.out.println("Delimiter switch to " + _dlimit);
							_dlimitSwitch = true;
						}*/
						String stmt = "";
						if(sqlStmt.length() > _prompt.length()){
							stmt = sqlStmt.substring(sqlStmt.lastIndexOf(_prompt),sqlStmt.length());
							stmt = replaceAll(_secPmt, "", stmt, true);
							stmt = replaceAll(_prompt, "", stmt, true);
							stmt = replaceAll("\n", " ", stmt, true);
							if(stmt.contains("DELIMITER"+_dlimit)){
								_dlimitSwitch = true;
								// Syntax-Fehler, z.B: DELIMITER;
								append("ERROR:\nDELIMITER must be followed by a 'delimiter' character or string\n");
								//ERROR: 
								//	DELIMITER must be followed by a 'delimiter' character or string
							}
							if(stmt.contains("DELIMITER ")){
								stmt = replaceAll("DELIMITER ", "", stmt, true);
								_dlimit = stmt.trim();
								System.out.println("Delimiter switch to " + _dlimit);
								_dlimitSwitch = true;
							}
							// String fuer History ermitteln
							if(sqlStmt.length()> _caretPosAtPrompt){
								String hist = sqlStmt.substring(_caretPosAtPrompt,sqlStmt.length());
								/*if(hist.contains(_secPmt))
									hist = hist.substring(hist.lastIndexOf(_secPmt),hist.length());
								hist = replaceAll(_secPmt, "", hist, true);
								hist = replaceAll(_secPt2, "", hist, true);
								hist = replaceAll(_prompt, "", hist, true);
								hist = replaceAll(_prmpt2, "", hist, true);*/
								hist = replaceAll("\n", "", hist, true);
								_consHistory.add(hist.trim());
								_conHistIndex= _consHistory.size();
							}
						}
						
						if(temp.endsWith(_dlimit)){ /* Befehl */
							if(_dlimitSwitch){// Falls Delimiter geändert wurde, kein SQL-Statement ausführen
								_dlimitSwitch = false;
								stmt = replaceAll(_dlimit, ";", stmt, true); // wirklich notwendig???
							}
							else{
								if(_dlimit != ";")
									stmt = replaceAll(_dlimit, ";", stmt, true); // Standard-Delimiter
								System.out.println("SQL-Statement: " + stmt);
								_sqlStatement = stmt;
								executeStatement();
							}
							append(_prompt);
							_caretPosAtPrompt = getCaretPosition();
						}
						else{
							//System.out.println("Test:" + temp);
							if(temp.endsWith(_prmpt2)){
								append(_prompt);
								_caretPosAtPrompt = getCaretPosition();
							}
							else{
								String te = getText();
								setText(te); // Cursor an Pos setzen (nicht schoen)
								append(_secPmt); 
								_caretPosAtPrompt = getCaretPosition();
								//_sqlEditor.setCaretPosition(_caretPos);
							}
						}					
						
					}
				}
				
			}
			});
	}
	public void setNewline(){
		append(_prompt);
		_caretPosAtPrompt = getCaretPosition();
	}
	public void resetConsole(boolean setFocus){
		if(isEditable()){
			setText(_prompt);
			if(setFocus)
				requestFocusInWindow();
			_caretPosAtPrompt = _prompt.length();
		}
	}
	public void setPrompt(String first, String second){
		if(first.length() > 2 && first.length() == second.length()){
			_prompt = first;
			_secPmt = second;
			_prmpt2 = _prompt.substring(0, _prompt.length()-1);
			//_secPt2 = _secPmt.substring(0, _secPmt.length()-1);
			resetConsole(true);
		}
	}
	public String getSqlStatement(){
		return _sqlStatement;
	}
	public String replaceAll(String findtxt, String replacetxt, String str, 
	        boolean isCaseInsensitive) {
	    if (str == null) {
	        return null;
	    }
	    if (findtxt == null || findtxt.length() == 0) {
	        return str;
	    }
	    if (findtxt.length() > str.length()) {
	        return str;
	    }
	    int counter = 0;
	    String thesubstr = "";
	    while ((counter < str.length()) 
	            && (str.substring(counter).length() >= findtxt.length())) {
	        thesubstr = str.substring(counter, counter + findtxt.length());
	        if (isCaseInsensitive) {
	            if (thesubstr.equalsIgnoreCase(findtxt)) {
	                str = str.substring(0, counter) + replacetxt 
	                    + str.substring(counter + findtxt.length());
	                // Failing to increment counter by replacetxt.length() leaves you open
	                // to an infinite-replacement loop scenario: Go to replace "a" with "aa" but
	                // increment counter by only 1 and you'll be replacing 'a's forever.
	                counter += replacetxt.length();
	            } else {
	                counter++; // No match so move on to the next character from
	                           // which to check for a findtxt string match.
	            }
	        } else {
	            if (thesubstr.equals(findtxt)) {
	                str = str.substring(0, counter) + replacetxt 
	                    + str.substring(counter + findtxt.length());
	                counter += replacetxt.length();
	            } else {
	                counter++;
	            }
	        }
	    }
	    return str;
	}
    public void addListener(ConsoleListener listener) {
        listeners.add(listener);
    }
    void executeStatement(){
        for(ConsoleListener listener : listeners){
            listener.executeConsoleStatement();
        }
    }
}
