package ds.swingutils;

import java.awt.Font;

import javax.swing.JOptionPane;


/**
 * open a popup info window (modal)
 *
 */
public class MessageBox {

	public MessageBox () {
	}
	
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
	
	public void showMessage(String message, String titel) {
		JOptionPane.showMessageDialog(null, message ,titel, JOptionPane.INFORMATION_MESSAGE);
	}

}
